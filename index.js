import {AppRegistry, YellowBox} from "react-native";

import App from "./src/app/App";

YellowBox.ignoreWarnings(["Warning:", "Module RCTImageLoader"]);

AppRegistry.registerComponent("forexbds", () => App);
