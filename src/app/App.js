/* eslint-disable */
import Loading from "__src/components/Loading";
import React, {PureComponent} from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "remote-redux-devtools";
import {createLogger} from "redux-logger";
import * as reduxStorage from "redux-storage";
import debounce from "redux-storage-decorator-debounce";
import createEngine from "./AsyncStorage";
import AppReducer from "./reducers";
import AppNavigator from "./navigator";
const logger = createLogger({
	// ...options
});

const engine = debounce(createEngine("pickup-key"), 1500);
const storage = reduxStorage.createMiddleware(engine);

const composeEnhancers = composeWithDevTools({ realtime: true });
const store = createStore(reduxStorage.reducer(AppReducer),
	composeEnhancers(applyMiddleware(thunk, storage, logger))
);

export default class App extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			rehydrated: false,
		};
	}

	componentDidMount() {
		// AsyncStorage.clear();
		reduxStorage.createLoader(engine)(store)
			.then((state) => {
				// console.log("Loaded previous state: ", state);
				this.setState({
					rehydrated: true
				});
			})
			.catch((e) => {
				console.log("Unable to restore previous state!", e)
			});
	}

	render(){

		if (!this.state.rehydrated) {
			return (
				<Loading />
			);
		}
		
		return (
			<Provider store={store}>
				<AppNavigator />
			</Provider>
		);
	}
}
