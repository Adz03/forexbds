/* eslint-disable */
import React, { PureComponent } from "react";
import {Platform} from "react-native";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { bindActionCreators } from "redux";
import { createReactNavigationReduxMiddleware, createNavigationReducer,
} from "react-navigation-redux-helpers";
import Loading from "../components/Loading";
import AsyncStorage from "@react-native-community/async-storage";

import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';
import Login from "../modules/login";
import Forgot from "../modules/forgot";
import Home from "../modules/home";

const AppNavigator = createStackNavigator({
	// QRSCAN: {
	// 	screen: QRSCAN,
	// },
	...Login,
	...Home,
	...Forgot,
},{
	mode: "card",
	headerLayoutPreset: 'center',
	transitionConfig: Platform.OS === "android" ? getSlideFromRightTransition : null,
	defaultNavigationOptions: {
		headerBackTitle: null,
		gesturesEnabled: false
	},

});

const AppContainer = createAppContainer(AppNavigator);

export const nav = createNavigationReducer(AppNavigator);
export const reactNavigation = createReactNavigationReduxMiddleware(
	"root",
	({ nav }) => nav,
);

const mapStateToProps = ({ nav }) => ({
	nav,
});

const mapDispatchToProps = (dispatch) => ({
	db: bindActionCreators({}, dispatch),
});

const persistenceKey = "persistenceKey"
const persistNavigationState = async (navState) => {
  try {
    await AsyncStorage.setItem(persistenceKey, JSON.stringify(navState))
  } catch(err) {
    // handle the error according to your needs
  }
}
const loadNavigationState = async () => {
	const jsonString = await AsyncStorage.getItem(persistenceKey);
	
  return JSON.parse(jsonString)
}

class App extends React.PureComponent{
	render(){
		return(
			<AppContainer persistNavigationState={persistNavigationState} 
				loadNavigationState={loadNavigationState} renderLoadingExperimental={() => <Loading />} >

			</AppContainer>
		)
	}
}

// const App = () => <AppContainer persistNavigationState={persistNavigationState} 
// 	loadNavigationState={loadNavigationState} renderLoadingExperimental={() => <Loading />} />;

export default connect(mapStateToProps, mapDispatchToProps)(App);
