/* eslint-disable */
import { combineReducers } from "redux";
import { nav } from "./navigator";
import { login } from "../modules/login";
import { forgot } from "../modules/forgot";
import { home } from "../modules/home";

const reducer = combineReducers({
	nav,
	login,
	forgot,
	home,
});


const rootReducer = (state = {}, action) => {
	switch (action.type) {
	case "REDUX_STORAGE_LOAD": {
		const newState = {...state};
		// newState.home.setDuplicateData = {};
		newState.home.imageLoad = false;

		return newState;
	}
	
	default:
		return reducer(state, action);
	}
};

export default rootReducer;
