/**
 * Attaches an event listener that handles the android-only hardware
 * back button
 * @param  {Function} callback The function to call on click
 * @param  {String} eventName The function to call on click
 */
import {BackHandler} from "react-native";

const handleAndroidBackButton = (eventName, callback) => {
	BackHandler.addEventListener(eventName, () => {
		callback();
		
		return true;
	});
};
/**
 * Removes the event listener in order not to add a new one
 * every time the view component re-mounts
 */
const removeAndroidBackButtonHandler = (eventName) => {
	BackHandler.removeEventListener(eventName, () => {});
};

export {handleAndroidBackButton, removeAndroidBackButtonHandler};
