/* eslint-disable */
import React, {PureComponent} from "react";
import Resource from "__src/resources";
import Loading from "__src/components/Loading";
import {View, Text, Image, TouchableOpacity, TextInput, StyleSheet} from "react-native";
import PropTypes from "prop-types";
import {Icon} from "react-native-elements";
import _ from "lodash";
const {Color, Res} = Resource;

export default class TxtInput extends PureComponent{
	setFocus(f){
		this.focus = f;
	}

	borderInput(){
		const {returnKeyType, value, onRef, onChangeText, onPass, isClose,
			refname, style, isFocus, err, style3, inputStyles} = this.props;
		const error = err ? {color: Color.red, borderColor: Color.red} : {color: Color.Standard};
		const style2 = isFocus ? {borderColor: Color.colorPrimary} : null;
		
		return (
			<View style={style}>
				<View accessible style={[styles.views_bi, style2, style3, error]}>
					<TextInput
						ref={(e) => onRef ? onRef(e) : null}
						autoCorrect={false}
						onBlur={this.props.onBlur}
						onFocus={this.props.onFocus}
						autoCapitalize="none"
						{...this.props}
						style={[styles.textfields2, inputStyles]}
						underlineColorAndroid="transparent"
						value={value}
						onChangeText={onChangeText}
						returnKeyType={returnKeyType}
						onSubmitEditing={() => {
							refname ? refname.focus() : null;
						}} />
					{isClose && !_.isEmpty(value) && <Icon onPress={onPass} name='close-o' type='evilicon'
							color="black" size={27} />}
				</View>
				{err ? <Text style={styles.errStyle}>{err}</Text> : null}
			</View>
		);
	}

	loginInput(){
		const {returnKeyType, value, onRef, onChangeText, viewPass, onSubmitEditing,
			style, isFocus, err, icon2, secureTextEntry, editable, placeholder, icon} = this.props;
		const error = err ? {color: Color.red, borderColor: Color.red} : {color: Color.Standard};
		const style2 = isFocus ? {borderColor: Color.colorPrimary} : null;
		
		return (
			<View style={style}>
				<View style={{flexDirection: "row", alignItems: "center"}}>
					<View accessible style={[styles.views_bi3, style2, error]}>
						<Image style={styles.image} source={Res.get(icon)}/>
						<TextInput
							ref={(e) => onRef ? onRef(e) : null}
							autoCorrect={false}
							onBlur={this.props.onBlur}
							onFocus={this.props.onFocus}
							autoCapitalize="none"
							placeholder={placeholder}
							style={styles.textfields}
							secureTextEntry={secureTextEntry}
							underlineColorAndroid="transparent"
							value={value}
							editable={editable}
							onChangeText={onChangeText}
							returnKeyType={returnKeyType}
							onSubmitEditing={onSubmitEditing} />
						{icon2 ? <TouchableOpacity onPress={viewPass}>
							<Image style={styles.image} source={Res.get(icon2)}/>
						</TouchableOpacity> : null}
					</View>
				</View>
				{err ? <Text style={styles.errStyle2}>{err}</Text> : null}
			</View>
		);
	}

	underlineInput(){
		const {label, err, onChangeText, style, returnKeyType, isText, secureTextEntry,
			value, inputStyles, onRef, style3, onSubmitEditing, isFocus} = this.props;
		const error = err ? {color: Color.red,
			borderBottomColor: Color.red} : {color: Color.Standard};
		const style2 = isFocus ? {borderBottomColor: Color.colorPrimary} : null;
		
		return (
			<View style={style}>
				<Text style={[styles.labelStyle, error]}>{label}</Text>
				<View accessible style={[styles.views_bi2, style2, style3, error]}>
					{isText ?
						<Text
							style={[styles.input, inputStyles]}>
							{value}
						</Text> : <TextInput
							ref={(e) => onRef ? onRef(e) : null}
							returnKeyType={returnKeyType}
							onBlur={this.props.onBlur}
							onFocus={this.props.onFocus}
							{...this.props}
							secureTextEntry={secureTextEntry}
							style={[styles.input, inputStyles]}
							onChangeText={onChangeText}
							autoCapitalize="none"
							value={value}
							underlineColorAndroid='transparent'
							onSubmitEditing={onSubmitEditing}/>}
					{this.renderComponent()}
				</View>
				{err ? <Text style={styles.errStyle}>{err}</Text> : null}
			</View>
			
		);
	}

	renderComponent(){
		switch (this.props.compName) {
		case "Loading":
			return (
				<View style={styles.load}>
					<Loading size="small" />
				</View>);
		case "Validated":
			return (
				<Icon
					name='check'
					type='evilicon'
					color='#2C932C'
					size={27}
				/>
			);
		case "Error":
			return (
				<Icon
					name='close-o'
					type='evilicon'
					color="red"
					size={27}
				/>
			);
		case "Date":
			return (
				<Icon
					name='calendar'
					type='evilicon'
					color="black"
					size={27}
				/>
			);
		case "ArrowDown":
			return (
				<Icon
					name='chevron-down'
					type='evilicon'
					color="black"
					size={27}
				/>
			);
		case "ArrowUp":
			return (
				<Icon
					name='chevron-up'
					type='evilicon'
					color="black"
					size={27}
				/>
			);

		case "Password":
			return (
				<TouchableOpacity onPress={this.props.onPass}>
					<Image style={styles.image2} source={Res.get("view_icon")}/>
				</TouchableOpacity>
			);
	
		default:
			return null;
		}
	}
	
	render(){
		const {round, logintype} = this.props;

		if (logintype){
			return (
				<View>
					{this.loginInput()}
				</View>
			);
		}
		
		return (
			<View>
				{ round ? this.borderInput() : this.underlineInput() }
			</View>
		);
	}
}

TxtInput.propTypes = {
	label: PropTypes.string, value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
	iconName: PropTypes.string, returnKeyType: PropTypes.string,
	viewPass: PropTypes.func, onChangeText: PropTypes.func,
	onRef: PropTypes.func, onPress: PropTypes.func,
	style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),  inputStyles: PropTypes.object,
	placeholder: PropTypes.string, refname: PropTypes.object,
	onFocus: PropTypes.func, icon: PropTypes.string,
	icon2: PropTypes.string, onBlur: PropTypes.func, isText: PropTypes.bool,
	round: PropTypes.bool, secureTextEntry: PropTypes.bool,
	logintype: PropTypes.bool, isFocus: PropTypes.bool, err: PropTypes.string,
	compName: PropTypes.string, onSubmitEditing: PropTypes.func,
	style3: PropTypes.oneOfType([PropTypes.object, PropTypes.array]), onPass: PropTypes.func, isClose: PropTypes.bool
};
TxtInput.defaultProps = {
	isClose: true
}
const styles = StyleSheet.create({
	labelStyle: {color: Color.Standard, fontSize: 15, fontFamily: "Roboto-Light"},
	errStyle: {color: Color.red, fontSize: 13, fontWeight: "normal", fontFamily: "Roboto-Light", marginTop: 4},
	errStyle2: {color: Color.red, fontSize: 13, width: "70%", alignSelf: "flex-start", 
		fontWeight: "normal", fontFamily: "Roboto-Light", marginTop: 4, textAlign: "left"},
	input: {flex: 1, fontFamily: "Roboto-Light", fontSize: 15, paddingVertical: 0, backgroundColor: Color.transparent},
	load: {width: 15, height: 15, margin: 7},

	// Round
	views_bi3: {flexDirection: "row", width: "100%", alignItems: "center", height: 40, backgroundColor: "white", borderColor: "#404040", borderWidth: 0.6, borderRadius: 4},
	views_bi2: {flexDirection: "row", backgroundColor: "transparent", alignItems: "center", width: "100%", height: 40,  borderBottomColor: "#404040", borderBottomWidth: 0.6},
	views_bi: {width: "100%", height: 40,  borderColor: "#404040", borderWidth: 0.6, borderRadius: 3,alignItems: "center", justifyContent: "center", flexDirection: "row", paddingHorizontal: 10},
	textfields: {flex: 1,  fontSize: 14, fontFamily: "Roboto-Light", marginLeft: 5, textAlignVertical: 'center', paddingVertical: 0, color: Color.Header },
	textfields2: { marginLeft: 5, flex: 1,  fontSize: 14, fontFamily: "Roboto-Light", textAlignVertical: 'center', paddingVertical: 0, color: Color.Header },
	image: {width: 18, height: 18, margin: 10},
	image2: {width: 18, height: 18, padding: 10},
});
