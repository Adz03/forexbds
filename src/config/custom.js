export const host = "https://api-beta.unified.ph";
// export const host = "https://apitestv3.gomigu.com";
export const port = 443;
export const ssl = false;
export const endpoint = "graphql";
