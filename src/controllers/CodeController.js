/* eslint-disable max-len */
import Message from "./Message";
import SQLite from "react-native-sqlite-storage";
import {Platform} from "react-native";

const storage = Platform.OS === "ios" ? "~data/forex-db.sqlite" : "~database/forex-db.sqlite";
const sqlite = SQLite.openDatabase({name: "forex-db", createFromLocation: storage});

export const createPickupDB = () => {
	return new Promise((resolve, reject) => {
		const msg = new Message();

		sqlite.transaction((tx) => {
			tx.executeSql(`CREATE TABLE IF NOT EXISTS pickup (
				id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
				invoice_number	TEXT,
				agent_id	TEXT,
				login_id	INTEGER,
				firstname TEXT,
				lastname TEXT,
				image_url1	TEXT,
				payment_check	FLOAT,
				payment_cash	FLOAT,
				payment_cc	FLOAT,
				pickup_fee	FLOAT,
				pickup_date	TEXT,
				promo_code	TEXT,
				sync	INTEGER,
				image_url2	TEXT,
				image_url3	TEXT,
				image_url4	TEXT,
				gps_coordinate TEXT,
				uuid TEXT
				)`, [], (tx, result) => {
				console.log("result", result);
				if (result.rowsAffected > 0 ){
					resolve({result: true, message: "Pickup is now created."});
				} else {
					resolve({result: false, message: "Pickup is already exist."});
				}
			}, (error) => {
				console.log("error", error);

				msg.result = false;
				msg.message = `${error}`;
				reject({result: msg.result, message: msg.message});
			});
		});
	});
};

export const checkIfExist = (item) => {
	return new Promise((resolve, reject) => {
		const msg = new Message();

		sqlite.transaction((tx) => {
			tx.executeSql("SELECT * FROM pickup WHERE invoice_number = ?", [item.invoice_number], (tx, result) => {
				if (result.rows.length >= 1 ){
					resolve(true);
				} else {
					resolve(false);
				}
			}, (error) => {
				msg.result = false;
				msg.message = `${error.message}`;
				reject(msg);
			});
		});
	});
};

export const insertPickup = (item) => {
	return new Promise((resolve, reject) => {
		const msg = new Message();

		sqlite.transaction((tx) => {
			tx.executeSql("SELECT * FROM pickup WHERE invoice_number = ?", [item.invoice_number], (tx, result) => {
				if (result.rows.length >= 1 ){
					tx.executeSql("UPDATE pickup SET firstname=?,lastname=?,image_url1=?,payment_check=?,payment_cash=?," +
						"payment_cc=?,pickup_fee=?,pickup_date=?,promo_code=?,sync=?,image_url2=?,image_url3=?," +
						"image_url4=?,gps_coordinate=? WHERE invoice_number= ?",
					[item.firstname, item.lastname, item.image_url1, item.payment_check, item.payment_cash,
						item.payment_cc, item.pickup_fee, item.pickup_date, item.promo_code, 0, item.image_url2,
						item.image_url3, item.image_url4, item.gps_coordinate, item.invoice_number], (tx, results) => {
						if ( results.rowsAffected > 0){
							resolve({result: true, message: "UPDATE success", results});
						} else {
							resolve({result: false, message: "UPDATE failed", results});
						}
					}, (error) => {
						msg.result = false;
						msg.message = `${error.message}`;
						reject(msg);
					});
				} else {
					tx.executeSql("INSERT INTO pickup(invoice_number,agent_id,login_id,firstname,lastname,image_url1,payment_check," +
						"payment_cash,payment_cc,pickup_fee,pickup_date,promo_code,sync,image_url2," +
						"image_url3,image_url4,gps_coordinate,uuid)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					[item.invoice_number, item.agent_id, item.login_id, item.firstname, item.lastname, item.image_url1, item.payment_check, item.payment_cash,
						item.payment_cc, item.pickup_fee, item.pickup_date, item.promo_code, 0, item.image_url2, item.image_url3, item.image_url4, item.gps_coordinate, item.uuid ], (tx, result) => {
						if (result.rowsAffected > 0 ){
							msg.result = true;
							msg.message = "Insert Succesfull";
							resolve(msg);
						} else {
							resolve({result: false, message: "Failed to INSERT"});
						}
					}, (error) => {
						msg.result = false;
						msg.message = `${error.message}`;
						reject(msg);
					});
				}
			}, (error) => {
				msg.result = false;
				msg.message = `${error.message}`;
				reject(msg);
			});
		});
	});
};

export const getPickupList = ({login_id, agent_id}) => {
	console.log("getPickupList", login_id, agent_id);

	return new Promise((resolve, reject) => {
		const msg = new Message();

		sqlite.transaction((tx) => {
			tx.executeSql("SELECT * FROM pickup WHERE login_id = ? AND agent_id = ? AND sync = 0", [login_id, agent_id], (tx, result) => {
				if (result.rows.length >= 1){
					const item = [];

					for (let i = 0;i < result.rows.length; i++){
						item.push(result.rows.item(i));
					}
					
					resolve({result: item, message: "Data Found!"});
				} else {
					resolve({result: [], message: "No Data!"});
				}
			}, (error) => {
				msg.result = false;
				msg.message = `${error.message}`;
				reject({result: msg.result, message: msg.message});
			});
		});
	});
};

export const updatePickup = (invoice_number) => {
	return new Promise((resolve, reject) => {
		const msg = new Message();

		if (!invoice_number) {
			msg.result = false;
			msg.message = "Invalid data Input";
			resolve({result: msg.result, message: msg.message});
		}
		sqlite.transaction((tx) => {
			tx.executeSql("UPDATE pickup SET sync = ? WHERE invoice_number = ?",
				[1, invoice_number], (tx, results) => {
					if ( results.rowsAffected > 0){
						resolve({result: true, message: "Success to UPDATE"});
					} else {
						resolve({result: false, message: "Failed to UPDATE"});
					}
				}, (error) => {
					msg.result = false;
					msg.message = `${error.message}`;
					reject({result: msg.result, message: msg.message});
				});
		});
	});
};

export const updateInvoiceById = (item) => {
	console.log("updateInvoiceById", item);
	
	return new Promise((resolve, reject) => {
		const msg = new Message();

		sqlite.transaction((tx) => {
			tx.executeSql("UPDATE pickup SET image_url1=?,payment_check=?,payment_cash=?," +
			"payment_cc=?,pickup_fee=?,pickup_date=?,promo_code=?,sync=?,image_url2=?,image_url3=?," +
			"image_url4=? WHERE id= ?",
			[item.image_url1, item.payment_check, item.payment_cash,
				item.payment_cc, item.pickup_fee, item.pickup_date, item.promo_code, 0, item.image_url2,
				item.image_url3, item.image_url4, item.id], (tx, results) => {
				if ( results.rowsAffected > 0){
					resolve({result: true, message: "Success to update invoice"});
				} else {
					reject({result: false, message: "Failed to update invoice", results});
				}
			}, (error) => {
				msg.result = false;
				msg.message = `${error.message}`;
				reject({result: msg.result, message: msg.message});
			});
		});
	});
};

export const deleteRecord = (id) => {
	return new Promise((resolve, reject) => {
		const msg = new Message();

		sqlite.transaction((tx) => {
			tx.executeSql("DELETE FROM pickup WHERE id = ?",
				[id], (tx, results) => {
					if ( results.rowsAffected > 0){
						resolve({result: true, message: "Success to DELETED"});
					} else {
						reject({result: false, message: "Failed to DELETED", results});
					}
				}, (error) => {
					msg.result = false;
					msg.message = `${error.message}`;
					reject({result: msg.result, message: msg.message});
				});
		});
	});
};
