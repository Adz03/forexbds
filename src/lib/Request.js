import qs from "qs";
import {encode as btoa} from "base-64";
import axios from "axios";

class Request {
	constructor(host, apiSecret) {
		this.host = host;
		this.secret = apiSecret;
	}

  setToken = (token) => {
  	this.token = token;
  };

  get = (route) => this._request(route, "GET");

  post = (route, body) => this._request(route, "POST", body);

  patch = (route, body) => this._request(route, "PATCH", body);

  delete = (route) => this._request(route, "DELETE");

  _request = async (route, method, body) => {
  	try {
  		console.debug(method, this.host, route);
  		const payload = {
  			method,
  			headers: {
  				Authorization: `Basic ${btoa("cquoin:1234")}`,
  				// "Content-Type": "application/json",
  				"Content-Type": "application/x-www-form-urlencoded",
  				Accept: "application/json",
  				"x-api-key": "wewe",
  			},
  		};

  		if (method !== "GET" && method !== "HEAD") {
  			// payload.body = JSON.stringify(body);
				
  			// const params = new FormData();

  			// params.append("inv", `AP-${invoicenum}`);
  			// params.append("avatar", {type: "image/jpg", uri: source, name: `AP-${invoicenum}.jpg`});
  			payload.body = qs.stringify(body);
  			// payload.data = JSON.stringify(body);
				
  		}

  		const url = `${this.host}${route}`;

  		return this._sendHttpRequest(url, payload);
  	} catch (e) {
  		throw e;
  	}
  };
	
	_sendHttpRequest2 = async (url, payload) => {
		payload.url = url;
		// payload.crossDomain = true;
		console.log("REQUEST PAYLOAD");
		console.log(payload);

		const response = await axios(payload);

		if (response.status === 200 || response.status === 201 ||
			response.status === 202 || response.status === 203){
			return await response.data;
		}

		throw await {code: response.status,
			data: response.data};
	}

  _sendHttpRequest = async (url, payload) => {
  	console.log("REQUEST PAYLOAD");
  	console.log(JSON.stringify({url, payload}));
  	const response = await fetch(url, payload);

  	if (response.status !== 200) {
  		throw await response.json();
  	}

  	return await response.json();
  };
}

export default Request;
