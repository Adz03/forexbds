import * as globals from "../../globals";

const API = {
	login: async (params) => {
		try {
			const result = await globals.APICALL.post("/login", {...params});

			return result;
		} catch (e) {
			console.log("login", JSON.stringify(e));
			throw e;
		}
	},

	callGet: async (routes) => {
		try {
			const result = await globals.APICALL.get(`${routes}`);

			return result;
		} catch (e) {
			console.log("callGet", JSON.stringify(e));
			throw e;
		}
	},

	callPost: async (routes, params) => {
		try {
			const result = await globals.APICALL.post(`${routes}`, {...params});

			return result;
		} catch (e) {
			console.log("callPost", JSON.stringify(e));
			throw e;
		}
	},
};

export default API;
