import * as Types from "./types";
import LifeLineAPI from "./api/index";

export const setParams = (data) => ({
	type: Types.SET_PARAMS,
	data,
});

export const reset = () => ({
	type: Types.RESET_FORGOT,
});

export const resetPassoword = (params) => (
	async(dispatch) => {
		try {

			dispatch({ type: Types.FORGOT_PROGRESS});

			const result = await LifeLineAPI.resetPassoword(params);

			console.log("result", result);

			if (result.status === 200 && result){
				dispatch({ type: Types.FORGOT_SUCCESS, data: result});
			} else {
				dispatch({ type: Types.FORGOT_FAILED, error: result.message});
			}
		} catch (error) {
			dispatch({ type: Types.FORGOT_FAILED, error});
		}
	}
);
