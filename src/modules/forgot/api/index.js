import * as globals from "__src/globals";

const APICALL = {
	resetPassoword: async (param ) => {
		try {
			const result = await globals.APICALL.post("/password/generate-reset-password",
				{ ...param });

			return result;
		} catch (e) {
			throw e;
		}
	},
};

export default APICALL;
