/* eslint-disable max-len */
import React from "react";
import {View, Text, StyleSheet, Image, ScrollView, StatusBar} from "react-native";
import TxtInput from "__src/components/TxtInput";
import Button from "__src/components/Button";
import _ from "lodash";
import validator from "validator";
import PropTypes from "prop-types";
import Resources from "__src/resources";
const {Color, Res} = Resources;

class ForgotScreen extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			fnUN: false,
			fnEmail: false,
			username: "",
			emailAddress: "",
			error: {},
		};
	}
  
  onPress = () => {
  	const {actions} = this.props;
  	const {username, emailAddress} = this.state;
  	const error = {};

  	if (_.isEmpty(username)){
  		error.username = "This field is required.";
  	} else	if (_.isEmpty(emailAddress)){
  		error.emailAddress = "This field is required.";
  	} else if (!validator.isEmail(emailAddress)){
  		error.emailAddress = "Email address is not valid";
  	}
    
  	this.setState({error});

  	if (_.isEmpty(error)){
  		const params = {};

  		params.username = username;
  		params.emailAddress = emailAddress;
  		params.from = "mobile";
      
  		actions.resetPassoword(params);
  	}
  }
  
  reset = () => {
  	const {actions} = this.props;

  	this.setState({username: "", emailAddress: ""}, () => {
  		actions.reset();
  	});
  }

  renderSuccess() {
  	const {emailAddress} = this.state;
    
  	return (
  		<View style={styles.flex1}>
  			<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
  			<View style={[styles.flex1, styles.padH25]}>
  				<View style={styles.marT30}>
  					<Text style={styles.title}>Successful!</Text>
  					<Image style={styles.imgSuccess} source={Res.get("check_icon")} resizeMode={"contain"} />
  				</View>
  				<Text style={[styles.txthassent, styles.marT30]}>
             We have sent an email to {`${emailAddress.substring(0, 2)}****${emailAddress.substring(emailAddress.length - 5)}`}.
             Click the link in the email to reset your password.
  				</Text>
  			</View>
  			<View style={styles.marB30}>
  				<Button
  					onPress={this.reset}
  					style={styles.btnCancel}
  					style2={styles.btnCancel}
  					label="Ok"
  					labelStyle={styles.labelStyle}/>
  			</View>
  		</View>
  	);
  }

  onBack = () => {
  	const {navigation} = this.props;

  	navigation.goBack();
  }


  render(){
  	const {forgot: {isForgotLoading, isForgotSuccess}} = this.props;
  	const {username, emailAddress, error} = this.state;
    
  	if (isForgotSuccess){
  		return this.renderSuccess();
  	}

  	return (
  		<View style={styles.flex1}>
  			<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
  			<ScrollView keyboardShouldPersistTaps="handled" style={styles.view1}>
  				<Text style={styles.txt1}>Did you forget your password?</Text>
  				<Text style={styles.txt2}>
          Enter your username and email address you're using for your account below and we will send you a password reset link
  				</Text>

  				<TxtInput
  					onFocus={() => this.setState({fnUN: true})}
  					onBlur={() => this.setState({fnUN: false})}
  					isFocus={this.state.fnUN}
  					value={username}
  					returnKeyType='next'
  					err={error.username}
  					label='Username'
  					style={styles.marT30}
  					onChangeText={(e) => this.setState({username: e})} />

  				<TxtInput
  					onFocus={() => this.setState({fnEmail: true})}
  					onBlur={() => this.setState({fnEmail: false})}
  					isFocus={this.state.fnEmail}
  					value={emailAddress}
  					returnKeyType='next'
  					err={error.emailAddress}
  					keyboardType="email-address"
  					label='Email Address'
  					style={styles.marT15}
  					onChangeText={(e) => this.setState({emailAddress: e})} />

  				<View style={styles.viewLogin}>
  					<Button
  						onPress={this.onPress}
  						style={styles.btnStyle}
  						loading={isForgotLoading}
  						label="Request Reset Link"/>

  					<Button
  						onPress={this.onBack}
  						style={styles.btnCancel}
  						style2={styles.btnCancel}
  						label="Back to login"
  						labelStyle={{color: Color.colorPrimaryDark}}/>
  				</View>
  			</ScrollView>
				
  		</View>
  	);
  }
}

ForgotScreen.propTypes = {
	actions: PropTypes.object,
	forgot: PropTypes.object,
	navigation: PropTypes.object,
};

const styles = StyleSheet.create({
	flex1: {flex: 1},
	padH25: {paddingHorizontal: 25},
	view1: {flex: 1, marginTop: 40, paddingHorizontal: 25},
	viewLogin: {marginTop: 30 },
	btnStyle: {height: 45, width: "100%"},
	btnCancel: {backgroundColor: Color.transparent, color: Color.colorPrimaryDark,
		marginHorizontal: 30, borderBottomWidth: 0, marginBottom: 5, marginTop: 15},
	txt1: {fontFamily: "Roboto", fontSize: 22, color: Color.Standard2, textAlign: "center"},
	txt2: {fontFamily: "Roboto-Light", fontSize: 15, color: Color.Standard, textAlign: "center", marginTop: 10},
	marT30: {marginTop: 30},
	marT15: {marginTop: 15},
	marB30: {marginBottom: 30},
  
	title: {textAlign: "center", fontSize: 25, fontFamily: "Roboto-Light", color: Color.Standard2},
	// txtsuccess: {textAlign: "center", color: Color.Standard2, fontSize: 25, fontWeight: "500", fontFamily: "Roboto-Light"},
	imgSuccess: {width: 130, height: 130, alignSelf: "center", marginVertical: 15 },
	txthassent: {fontFamily: "Roboto-Light", textAlign: "center",  fontSize: 15, color: Color.Standard2},
  
	labelStyle: {color: Color.colorPrimaryDark, fontSize: 18},
});

export default ForgotScreen;
