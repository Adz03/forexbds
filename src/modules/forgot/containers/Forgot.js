import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import ForgotScreen from "../components/ForgotScreen";

const mapStateToProps = ({ login, forgot }) => ({
	login,
	forgot,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(ActionCreators, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotScreen);
