import Forgot from "./containers/Forgot";
import navigationOptions from "__src/components/navOpt";

import reducers from "./reducers";

export const forgot = reducers;
export default {
	Forgot: {
		screen: Forgot,
		navigationOptions,
	},
};
