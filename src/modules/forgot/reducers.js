import { combineReducers } from "redux";

import * as Types from "./types";

const setParams = (state = {}, action) => {
	switch (action.type){
	case Types.SET_PARAMS:
		return action.data;
	default:
		return state;
	}
};

const isForgotLoading = (state = false, action) => {
	switch (action.type){
	case Types.FORGOT_PROGRESS:
		return true;
	case Types.FORGOT_FAILED:
	case Types.FORGOT_SUCCESS:
	case Types.RESET_FORGOT:
		return false;
	default:
		return state;
	}
};

const isForgotSuccess = (state = false, action) => {
	switch (action.type){
	case Types.FORGOT_SUCCESS:
		return true;
	case Types.FORGOT_FAILED:
	case Types.FORGOT_PROGRESS:
	case Types.RESET_FORGOT:
		return false;
	default:
		return state;
	}
};

const ForgotData = (state = {}, action) => {
	switch (action.type){
	case Types.FORGOT_SUCCESS:
		return action.data;
	case Types.FORGOT_FAILED:
	case Types.FORGOT_PROGRESS:
	case Types.RESET_FORGOT:
		return {};
	default:
		return state;
	}
};

const ForgotFailed = (state = "", action) => {
	switch (action.type){
	case Types.FORGOT_FAILED:
		return action.error;
	case Types.FORGOT_SUCCESS:
	case Types.FORGOT_PROGRESS:
	case Types.RESET_FORGOT:
		return "";
	default:
		return state;
	}
};


export default combineReducers({
	setParams,
	isForgotLoading,
	isForgotSuccess,
	ForgotData,
	ForgotFailed,
});
