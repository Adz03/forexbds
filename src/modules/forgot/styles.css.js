import {StyleSheet} from "react-native";
import {getStatusBarHeight} from "__src/resources/customize/StatusBarHeight";
import Color from "__src/resources/styles/color";

export default StyleSheet.create({
	container: {flexShrink: 1, width: "100%", height: "100%", backgroundColor: Color.Standard2},
	body: {flex: 1, marginTop: getStatusBarHeight(true), backgroundColor: Color.white},
	imagebackground: {width: "100%", height: "100%", marginTop: getStatusBarHeight(true), position: "absolute"},
	
	// LoginForm
	flex1: {flex: 1},
	flexRow: {flexDirection: "row"},
	txtLanguage: {color: "white", fontFamily: "Roboto-Light", fontSize: 13, marginRight: 3},
	marginTop10: {marginTop: 10},
	height90: {height: "93%"},

	top: {flex: 1, height: "45%", alignItems: "center",  justifyContent: "center"},
	middle: {flex: 1, height: "40%", paddingHorizontal: 25, justifyContent: "center"},
	bottom: {flex: 1,  height: "15%" },


	txtError: { color: "red", fontFamily: "Roboto-Light", fontSize: 15, textAlign: "center", marginBottom: 5},
	txtHelpContainer: {fontSize: 13, fontFamily: "Roboto-Light", marginVertical: 10, color: Color.white, textAlign: "center" },
	txtHelp: {fontWeight: "bold", color: Color.colorPrimary},
	txtGetHelpContainer: {fontSize: 14, marginVertical: 20, color: Color.colorPrimary, fontFamily: "Roboto-Light", textAlign: "center" },
	txtGetHelpContainer2: {fontSize: 14, marginVertical: 20, color: Color.Standard2, fontFamily: "Roboto-Light", textAlign: "center" },
	txtfb: {color: Color.blue, fontSize: 15, fontFamily: "Roboto-Light"},
	btnfb: {height: 45, marginTop: 15, alignItems: "center",
		backgroundColor: "transparent", flexDirection: "row", justifyContent: "center" },
	loadview: {width: 10, height: 10},

	btnlogin: {height: 45, borderBottomWidth: 6, width: 100},
	btncancel: {height: 45, borderBottomWidth: 6, width: 100, backgroundColor: Color.white,
		borderColor: Color.colorPrimaryDark, borderWidth: 1},


	viewLogin: {flex: 1, marginTop: 15, paddingHorizontal: 25, flexDirection: "row", justifyContent: "space-evenly"},

	// LandScape
	flexDirRow: {flexDirection: "row"},
	row1: {flex: 1, alignItems: "center",  justifyContent: "center"},
	row2: {flex: 1, alignItems: "center",  justifyContent: "center", paddingHorizontal: 20},
	txtLifeline: {fontFamily: "Roboto", fontSize: 75, color: Color.black},
	buttonWrapper: { marginTop: 15, paddingHorizontal: 25, flexDirection: "row", justifyContent: "space-evenly"},

	// renderError
	inpuView1: {justifyContent: "center", backgroundColor: Color.lightred, padding: 7, marginTop: 10, flexDirection: "row"},
	iconContainerStyle: {position: "absolute", left: 5, top: 8},
	txt3: {textAlign: "center", fontFamily: "Roboto-Light", color: Color.Standard2, fontSize: 13},
})
;
