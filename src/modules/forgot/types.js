
export const FORGOT_PROGRESS = "forgot/types/FORGOT_PROGRESS";
export const FORGOT_SUCCESS = "forgot/types/FORGOT_SUCCESS";
export const FORGOT_FAILED = "forgot/types/FORGOT_FAILED";

export const SET_PARAMS = "forgot/types/SET_PARAMS";
export const RESET_FORGOT = "forgot/types/RESET_FORGOT";

