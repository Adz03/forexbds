/* eslint-disable */
import * as Types from "./types";
import { Alert } from "react-native";
import RNFetchBlob from "rn-fetch-blob";
import api from "../api/index";
import _ from "lodash";
const BASE_URL = "https://api2.forexcargo.us/";

import * as Schema from "../../controllers/CodeController";
// import AWS from "aws-sdk/dist/aws-sdk-react-native";

export const setCamera = (data) => ({
	type: Types.SET_CAMERA,
	data,
});

export const imageLoad = (data) => ({
	type: Types.IMAGE_LOADING,
	data,
});

export const addImage = (data) => ({
	type: Types.ADD_IMAGE,
	data,
});

export const uploadInvoice = (data) => ({
	type: Types.UPLOAD_IMAGE,
	data,
});

export const resetImage = () => ({
	type: Types.RESET_IMAGE
});
export const resetCollection = () => ({
	type: Types.RESET_FORPICKUP
});

export const setInput = (data) => ({
	type: Types.SET_INPUT,
	data
});

export const logout = () => ({
	type: Types.LOGOUT
});

export const saveLatLong = (data) => ({
	type: Types.SAVE_LAT_LONG,
	data,
});

export const setUUID = (data) => ({
	type: Types.SET_UNIQUE_UUID,
	data,
});

export const getPickupDetails = (params, navigation) => (
	async(dispatch) => {
		try {
			dispatch({ type: Types.GET_PICKUP_DETAILS_PROGRESS});

			const response = await api.callPost("/agent_pickup_get_details",params);
			console.log("response", response);

			if (response && response.status === 1){

				dispatch(setInput({ ...response.result[0], forupdate: true}));
				navigation.navigate("ViewDetails", {title: "Received Boxes"});
			}else{
				throw response;
			}
		} catch (error) {
			Alert.alert("Notice", error.result || `Something went wrong (001) ${JSON.stringify(error)}`);
			dispatch({ type: Types.GET_PICKUP_DETAILS_FAILED, error});
		}
	}
);

export const addInvoiceOffline = (data) => ({
	type: Types.INPUT_FORPICKUP_OFFLINE,
	data
});
export const isSubmitViaOffline = () => ({
	type: Types.INPUT_FORPICKUP_OFFLINE_PROGRESS
});

export const submitViaOfflineFailed = (error) => ({
	type: Types.INPUT_FORPICKUP_OFFLINE_FAILED, error
});

export const submitPickupViaOffline = (params, type) => (
	async(dispatch) => {
		try {
			dispatch({ type: Types.INPUT_FORPICKUP_OFFLINE_PROGRESS});

			const isTrue = await Schema.checkIfExist(params);
			console.log("submitDeliveryViaOffline", isTrue, params, type);

			if(isTrue && _.isEmpty(type)){
				dispatch({ type: Types.INPUT_FORPICKUP_OFFLINE_FAILED, error: "Invoice Number Already Exist"});
			}else{
				const res = await Schema.insertPickup(params);

				if(res){
					const response = await Schema.getPickupList(params);
					const newParams = {
						firstname: params.firstname,
						lastname: params.lastname
					}
					dispatch({ type: Types.INPUT_FORPICKUP, data: params});
					dispatch(setInput(newParams));
					dispatch({type: Types.INPUT_FORPICKUP_OFFLINE, data: response.result, response});
				}
			}
		} catch (error) {
			dispatch({ type: Types.INPUT_FORPICKUP_OFFLINE_FAILED, error: error.result || `Something went wrong (01) ${JSON.stringify(error)}`, error2: error});
		}
	}
);

export const updateExisting = (params) => (
	async(dispatch) => {
		try {
			const res = await Schema.updateInvoiceById(params);

			if(res){
				const response = await Schema.getPickupList(params);
				dispatch({ type: Types.INPUT_FORPICKUP, data: params});
				dispatch({type: Types.INPUT_FORPICKUP_OFFLINE, data: response.result, response});
			}
		} catch (error) {
			dispatch({ type: Types.INPUT_FORPICKUP_OFFLINE_FAILED, error: error.result || `Something went wrong (01) ${JSON.stringify(error)}`, error2: error});
		}
	}
);

export const InvoiceReupload = (params) => (
	async(dispatch) => {
		try {
			// if(params){
			// 	dispatch({ type: Types.SET_TRACKING_INVOICE, data: params.invoice_number, params});

			// 	return;
			// }
			dispatch({ type: Types.REUPLOAD_INVOICE_PROGRESS});
			dispatch({ type: Types.SET_TRACKING_INVOICE, data: params.invoice_number});

			if(params.image_url1){
				const img_url = await uploadImage(params.image_url1, `AP-${params.invoice_number}`);
				console.log("img_url", img_url, params.image_url1, `AP-${params.invoice_number}`);

				params.image_url1 = img_url && img_url.status === 1 ? `${img_url.result}` : null;
			}

			if(params.image_url2){
				const box_1 = await uploadImage(params.image_url2, `BOX1-${params.invoice_number}`);
				console.log("box_1", box_1, params.image_url2);

				params.image_url2 = box_1 && box_1.status === 1 ? `${box_1.result}` : null;
			}

			if(params.image_url3){
				const box_2 = await uploadImage(params.image_url3, `BOX2-${params.invoice_number}`);
				console.log("box_2", box_2, params.image_url3);

				params.image_url3 = box_2 && box_2.status === 1 ? `${box_2.result}` : null;
			}

			if(params.image_url4){
				const box_3 = await uploadImage(params.image_url4, `BOX3-${params.invoice_number}`);
				console.log("box_3", box_3, params.image_url4);

				params.image_url4 = box_3 && box_3.status === 1 ? `${box_3.result}` : null;
			}
			
			const result = await api.callPost("/agent_pickup_save",params);
			console.log("result", result);

			if (result){
				dispatch({ type: Types.REUPLOAD_INVOICE, data: params});
				const response = await Schema.updatePickup(params.invoice_number);
				if(response.result){
					const result = await Schema.getPickupList(params);
					if(result){
						dispatch(addInvoiceOffline(result.result));
					}
				}
			}
		} catch (error) {
			if(error.status === 0 || error.code === -7){
				const data = {
					...error,
					invoiceno: params.invoice_number,
					params
				}
				dispatch({ type: Types.SET_DATA_FOR_DUPLICATE, data});
			}else{
				dispatch({ type: Types.REUPLOAD_INVOICE_FAILED, error: error.result || "Something went wrong (Code 01)", error});
			}
		}
	}
);

export const deleteRecord = (params) => (
	async(dispatch) => {
		try {
			const response = await Schema.deleteRecord(params.id);

			console.log("response", response, params);
			if(response.result){
				const result = await Schema.getPickupList(params);
				if(result){
					dispatch({type: Types.DELETE_RECORD_FROM_SQL});
					dispatch(addInvoiceOffline(result.result));
				}
			}
		} catch (error) {
			console.log("deleteRecord", error);
		}
	}
);

export const ReplaceInvoice = (params) => (
	async(dispatch) => {
		try {
			dispatch({ type: Types.REPLACE_INVOICE_PROGRESS});
			dispatch({ type: Types.SET_TRACKING_INVOICE, data: params.invoice_number});

			if(params.image_url1){
				const img_url = await uploadImage(params.image_url1, `AP-${params.invoice_number}`);
				console.log("img_url", img_url, params.image_url1, `AP-${params.invoice_number}`);

				params.image_url1 = img_url && img_url.status === 1 ? `${img_url.result}` : null;
			}

			if(params.image_url2){
				const box_1 = await uploadImage(params.image_url2, `BOX1-${params.invoice_number}`);
				console.log("box_1", box_1, params.image_url2);

				params.image_url2 = box_1 && box_1.status === 1 ? `${box_1.result}` : null;
			}

			if(params.image_url3){
				const box_2 = await uploadImage(params.image_url3, `BOX2-${params.invoice_number}`);
				console.log("box_2", box_2, params.image_url3);

				params.image_url3 = box_2 && box_2.status === 1 ? `${box_2.result}` : null;
			}

			if(params.image_url4){
				const box_3 = await uploadImage(params.image_url4, `BOX3-${params.invoice_number}`);
				console.log("box_3", box_3, params.image_url4);

				params.image_url4 = box_3 && box_3.status === 1 ? `${box_3.result}` : null;
			}
			
			const result = await api.callPost("/agent_pickup_save",params);
			console.log("result", result);

			if (result){
				dispatch({ type: Types.REPLACE_INVOICE, data: params});
				const response = await Schema.updatePickup(params.invoice_number);
				if(response.result){
					const result = await Schema.getPickupList(params);
					if(result){
						dispatch(addInvoiceOffline(result.result));
					}
				}
			}
		} catch (error) {
			dispatch({ type: Types.REPLACE_INVOICE_FAILED, error: error.result || "Something went wrong (Code 01)", error});
		}
	}
);

export const fetchOffline = (params) => (
	async(dispatch) => {
		try {
			const result = await Schema.getPickupList(params);
			console.log("getPickupList", result);

			if (result && result.result.length > 0){
				dispatch(addInvoiceOffline(result.result));
			}
		} catch (error) {
			console.log("error", error);
		}
	}
);

export const fetchForPickup = (params, noload) => (
	async(dispatch) => {
		try {
			if(_.isEmpty(noload)){
				dispatch({ type: Types.GET_FORPICKUP_PROGRESS});
			}

			const response = await api.callPost("/agent_pickup_list",params);
			console.log("response", response);

			if (response){
				dispatch({ type: Types.GET_FORPICKUP, data: response.result.data});
				dispatch({ type: Types.GET_COUNT_PICKUP, data: _.toNumber(response.result.RecordTotal)});
			}
		} catch (error) {
			dispatch({ type: Types.GET_FORPICKUP_FAILED, error});
		}
	}
);

export const seeMorePickup = (params) => (
	async(dispatch) => {
		try {
			dispatch({ type: Types.GET_FORPICKUP_PROGRESS});

			const response = await api.callPost("/agent_pickup_list", params);
			console.log("response", response);

			if (response){
				dispatch({ type: Types.SEE_MORE_PICKUP, data: response.result.data});
				dispatch({ type: Types.GET_COUNT_PICKUP, data: _.toNumber(response.result.RecordTotal)});
			}
		} catch (error) {
			dispatch({ type: Types.GET_FORPICKUP_FAILED, error: error.result || error.message || "Something went wrong (Code 002)"});
		}
	}
);

const uploadImage = async(source, filename) => {
		let params = new FormData();
		params.append("inv", `${filename}`);
		params.append("avatar", {type: "image/jpg", uri: source, name: `${filename}.jpg`});

		const response = await fetch(`${BASE_URL}main/upload/`, {
			method: "POST",
			headers: {
				Authorization: `Basic Y3F1b2luOjEyMzQ=`,
				"x-api-key": "wewe",
			},
			body: params
		})

		if (response.status !== 200) {
			return null;
		}

		return await response.json();
};

export const uploadImage2 = (source, invoicenum) => async (dispatch) => {
	try {
		dispatch({ type: Types.UPLOAD_IMAGE_LOAD, data: source });

		let params = new FormData();
		params.append("inv", `AP-${invoicenum}`);
		params.append("avatar", {type: "image/jpg", uri: source, name: `AP-${invoicenum}.jpg`});

		fetch(BASE_URL, {
			method: "POST",
			headers: {
				Authorization: `Basic Y3F1b2luOjEyMzQ=`,
				"x-api-key": "wewe",
			},
			body: params
		}).then(res => res.json()).then(response => {
			if(response){
				console.log("response", JSON.stringify(response));
				dispatch({ type: Types.UPLOAD_IMAGE, data: `${BASE_URL}${response.result}` });
			}
		})
		.catch(error => { console.log("error", JSON.stringify(error));});
	} catch (error) {
		dispatch({ type: Types.UPLOAD_IMAGE_FAILED, error});
	}
};

export const uploadImage3 = (source, invoicenum) => async (dispatch) => {
	try {
		dispatch({ type: Types.UPLOAD_IMAGE_LOAD, data: source });

		let params = new FormData();
		params.append("inv", `AP-${invoicenum}`);
		params.append("avatar", {type: "image/jpg", uri: source, name: `AP-${invoicenum}.jpg`});

		RNFetchBlob.fetch('POST', BASE_URL, {
			// dropbox upload headers
			Authorization : "Basic Y3F1b2luOjEyMzQ=",
			"x-api-key": "wewe",
			// 'Content-Type' : 'application/octet-stream',
			// Change BASE64 encoded data to a file path with prefix `RNFetchBlob-file://`.
			// Or simply wrap the file path with RNFetchBlob.wrap().
		}, [{
			name: "avatart",
			// inv: invoicenum + ".jpg",
			filename: invoicenum + ".jpg",
			type : 'image/jpg',
			// data: "inv",
			data: `RNFetchBlob-${source}`
	}])
		.then((res) => {
			if(res){
				console.log("res", JSON.stringify(res), invoicenum);
				dispatch({ type: Types.UPLOAD_IMAGE, data: `${BASE_URL}${res.result}` });
			}
		})
		.catch((err) => {
			dispatch({ type: Types.UPLOAD_IMAGE_FAILED, err});
		})
	} catch (error) {
		dispatch({ type: Types.UPLOAD_IMAGE_FAILED, error});
	}
};
