/* eslint-disable no-inline-comments */
/* eslint-disable line-comment-position */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable max-len */
import React from "react";
import {View, ScrollView, TouchableOpacity, Image, Alert,
	SafeAreaView, Text, Dimensions, TextInput, Platform} from "react-native";
import styles from "../styles.css";
import ImagePicker from "react-native-image-picker";
import _ from "lodash";
import moment from "moment";
import {Icon} from "react-native-elements";
import PropTypes from "prop-types";
// import Button from "__src/components/Button";
import TxtInput from "__src/components/TxtInput";
import FullImageView from "./screen/FullImageView";
import LoaderModal from "__src/components/LoaderModal";
import Resource from "__src/resources";
import PaymentInfo from "./screen/PaymentInfo";
import CryptoJS from "crypto-js";
import Geolocation from "@react-native-community/geolocation";
import TransPass from "../../../components/TransPass";
import Box from "./screen/Box";
import ImageResizer from "react-native-image-resizer";
const {Res} = Resource;
const {width} = Dimensions.get("window");
const numColumns = 3;
const geoOptions = { enableHighAccuracy: true, timeout: 15000 };
const options = {
	title: "Choose method",
	storageOptions: {
		cameraRoll: true, skipBackup: true, waitUntilSaved: true,
		path: "images",
	},
};
const isIos = Platform.OS === "ios";

class DetailScreen extends React.PureComponent{
	constructor(props){
		super(props);
		this.state = {
			isFullView: false,
			error: {}, isShowPin: false,
			img: {},
		};
	}

	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: "Receive Boxes",
			headerRight: (
				<TouchableOpacity activeOpacity={0.7} onPress={navigation.getParam("done")}>
					<Text style={styles.txtLogout}>Save</Text>
				</TouchableOpacity>),
		};
	};

	componentDidMount() {
		const {navigation} = this.props;

		navigation.setParams({ done: this.onShowPin });
	}

	componentDidUpdate(prevProps){
		const {home: {invoiceImage, listOfOffline, submitViaOfflineFailed, setInput},
			actions, navigation} = this.props;

		if (!_.isEqual(prevProps.home.invoiceImage, invoiceImage) &&
			!_.isEmpty(invoiceImage)){
			const newInput = _.merge({}, setInput);

			newInput.image_url1 = invoiceImage.url;
			actions.setInput(newInput);
		}

		if (!_.isEqual(prevProps.home.invoiceImage, invoiceImage) &&
			!_.isEmpty(invoiceImage)){
			const newInput = _.merge({}, setInput);

			newInput.image_url1 = invoiceImage;
			actions.setInput(newInput);
		}
		
		if (!_.isEqual(prevProps.home.listOfOffline, listOfOffline) &&
			!_.isEmpty(listOfOffline)){
			this.setState({isShowPin: false});
			navigation.goBack();
		}

		if (!_.isEqual(prevProps.home.submitViaOfflineFailed, submitViaOfflineFailed) &&
			!_.isEmpty(submitViaOfflineFailed)){
			setTimeout(() => this.onFailed(submitViaOfflineFailed), 10);
		}
	}

	onFailed = (submitViaOfflineFailed) => {
		if (submitViaOfflineFailed === "Invoice Number Already Exist"){
			Alert.alert("Notice", `${submitViaOfflineFailed}. \n Do you want to replace the existing?`,
				[{
					text: "Cancel",
					onPress: () => console.log("Cancel Pressed"),
					style: "cancel",
				}, {text: "OK", onPress: () => this.onSubmit("edit")},
				], {cancelable: false});
		} else {
			Alert.alert("Notice", submitViaOfflineFailed);
		}
	}

	fetchForPickup = () => {
		const {actions, login: {session, setLoginCredential}} = this.props;
		const params = {
			login_id: session.result.user_id,
			agent_id: setLoginCredential.agent_id,
			order_column: 0,
			order_dir: "DESC",
			start: 0,
			length: 20,
			search_value: "",
		};
		
		actions.fetchOffline(params);
		actions.fetchForPickup(params);
	}

	onImageItemClick = (img) => {
		this.setState({isFullView: true, img});
	}

	renderItem = ({item, index}) => {
		if (item.empty === true) {
			return (<View key={`idx${index}`}
				style={[styles.itemContainer, styles.itemInvisible]} />);
		}
							
		return (
			<TouchableOpacity activeOpacity={0.7} key={`${index}`} onPress={() => this.onImageItemClick(item)}>
				<View elevation={5} style={[styles.itemContainer,
					{width: (width - 80) / numColumns, height: (width - 80) / numColumns}]}>
					<TouchableOpacity onPress={this.openGalery} style={styles.btnClose}>
						<Icon name="highlight-off" size={22} color="red"/>
					</TouchableOpacity>
					<Image style={styles.imageStyle} source={item.source} resizeMode="contain" />
				</View>
			</TouchableOpacity>
		);
	}
	
	onChangeText = (type) => (value) => {
		const {home: {setInput}, actions} = this.props;
		const newInput = _.merge({}, setInput);
		const error = {};

		if (_.isEmpty(value)){
			error[type] = "This field is required.";
		}
		newInput[type] = value;
		this.setState({error});
		actions.setInput(newInput);
	}

	onSubmit = (type = null) => {
		const {actions, login: {session},
			home: {setInput}} = this.props;
		const error = {};

		console.log("asfas", session.PIN, _.toString(CryptoJS.MD5(setInput.PIN)));

		if (_.isEmpty(setInput.PIN)){
			error.PIN = "This field is required.";
		} else if (session.PIN !== _.toString(CryptoJS.MD5(setInput.PIN))){
			error.PIN = "Invalid pin code.";
		}

		this.setState({error});

		if (_.isEmpty(error)){
			actions.isSubmitViaOffline();
			Geolocation.getCurrentPosition((e) => this.geoSuccess(e, type), this.geoFailure, geoOptions);
		}
	}

	geoSuccess = (position, type) => {
		const {actions, login: {session, setLoginCredential},
			home: {setInput, setUUID}} = this.props;
		const gps = _.has(position, "coords.latitude") ?
			`${position.coords.latitude},${position.coords.longitude}` : "";
		const params = {};

		params.invoice_number = setInput.invoice_number;
		params.agent_id = setLoginCredential.agent_id;
		params.login_id = session.result.user_id;
		params.firstname = setInput.firstname;
		params.lastname = setInput.lastname;
		params.image_url1 = setInput.image_url1;
		params.image_url2 = setInput.image_url2;
		params.image_url3 = setInput.image_url3;
		params.image_url4 = setInput.image_url4;
		params.payment_check = _.toNumber(setInput.paymen_check) || 0;
		params.payment_cash = _.toNumber(setInput.payment_cash) || 0;
		params.payment_cc = _.toNumber(setInput.payment_cc) || 0;
		params.pickup_fee = _.toNumber(setInput.pickup_fee) || 0;
		params.pickup_date = moment().format("YYYY-MM-DD HH:mm:ss");
		params.promo_code = setInput.promo_code;
		params.gps_coordinate = gps;
		params.uuid = setUUID;

		if (setInput.id){
			params.id = setInput.id;
			actions.isSubmitViaOffline();
			actions.updateExisting(params);
		} else {
			actions.submitPickupViaOffline(params, type);
		}
	};

	geoFailure = () => {
		const {actions} = this.props;

		actions.submitViaOfflineFailed("Unable to get location. Please enable your location in your settings.");
	};

	onShowPin = () => {
		const { home: {setInput} } = this.props;
		const error = {};

		if (_.isEmpty(setInput.invoice_number)){
			error.invoice_number = "Invoice number is required.";
			Alert.alert("Notice", error.invoice_number);
		}
		if (_.isEmpty(setInput.image_url1)){
			error.invoice_number = "Please upload a copy of your invoice.";
			Alert.alert("Notice", error.invoice_number);
		}
		if (_.isEmpty(`${setInput.payment_cash}`) && _.isEmpty(`${setInput.payment_cc}`) &&
			_.isEmpty(`${setInput.paymen_check}`)){
			error.payment = "Please fill atleast one of the following payment.";
			Alert.alert("Notice", error.payment);
		}
		if (_.isEmpty(setInput.firstname)){
			error.firstname = "This field is required.";
		}
		if (_.isEmpty(setInput.lastname)){
			error.lastname = "This field is required.";
		}

		this.setState({error});

		if (_.isEmpty(error)){
			this.setState({isShowPin: true});
		}
	}

	clearInvoice = () => {
		const {actions, home: {setInput}} = this.props;
		const newInput = _.merge({}, setInput);

		delete newInput.image_url1;

		actions.setInput(newInput);
		actions.uploadInvoice("");
	}

	clearBox = (item) => {
		const {actions, home: {setInput}} = this.props;
		const newInput = _.merge({}, setInput);

		delete newInput[item];

		actions.setInput(newInput);
		actions.uploadInvoice("");
	}

	TakeBoxes = (type) => {
		const {actions} = this.props;

		actions.setCamera(true);
		ImagePicker.showImagePicker(options, (response) => {
			if (response.didCancel) {
				actions.setCamera(false);
				actions.imageLoad(false);
			} else if (response.error) {
				actions.setCamera(false);
				actions.imageLoad(false);
			} else if (response.customButton) {
				actions.setCamera(false);
				actions.imageLoad(false);
			} else {
				console.log("result", response);
				actions.imageLoad(true);

				this.resize(response, type);
			}
		});
	}

	openGalery = () => {
		const {actions} = this.props;

		actions.setCamera(true);
		ImagePicker.showImagePicker(options, (response) => {
			if (response.didCancel) {
				actions.setCamera(false);
				actions.imageLoad(false);
			} else if (response.error) {
				actions.setCamera(false);
				actions.imageLoad(false);
			} else if (response.customButton) {
				actions.setCamera(false);
				actions.imageLoad(false);
			} else {
				console.log("result", response);
				actions.imageLoad(true);
				this.resize(response);
			}
		});
	}

	resize = (res, type) => {
		const {actions, home: {setInput}} = this.props;

		ImageResizer.createResizedImage(res.uri, res.width, res.height, "JPEG", 20)
			.then((response) => {
				const url = response.uri;
				const newInput = _.merge({}, setInput);

				if (type){
					newInput[`image_url${type}`] = url;
				} else {
					newInput.image_url1 = url;
				}
				actions.setInput(newInput);
				actions.imageLoad(false);

				console.log("response", response);
			})
			.catch((err) => {
				actions.imageLoad(false);

				return Alert.alert(
					"Unable to resize the photo",
					JSON.stringify(err),
				);
			});
	}

	render(){
		const {home: {isUploadingImage, isSubmitViaOffline, imageLoad,
			setInput}, navigation} = this.props;
		const {error, isShowPin} = this.state;

		const suffix = isIos ? `?${new Date()}` : "";
		const source = setInput.image_url1 ? {uri: `${setInput.image_url1}${suffix}`} : null;
		const box1 = setInput.image_url2 ? {uri: `${setInput.image_url2}${suffix}`} : null;
		const box2 = setInput.image_url3 ? {uri: `${setInput.image_url3}${suffix}`} : null;
		const box3 = setInput.image_url4 ? {uri: `${setInput.image_url4}${suffix}`} : null;
		const style1 = setInput.image_url2 ? styles.imageStyle : styles.addImage;
		const style2 = setInput.image_url3 ? styles.imageStyle : styles.addImage;
		const style3 = setInput.image_url4 ? styles.imageStyle : styles.addImage;

		return (
			<SafeAreaView style={[styles.container, {backgroundColor: "white"}]}>
				<ScrollView showsVerticalScrollIndicator={false} style={[styles.container, {backgroundColor: "white"}]}>
					<Text style={styles.txtLabel}>Invoice Number</Text>
					<View style={[styles.dView0, styles.marginTop5]}>
						<TextInput style={[styles.textfields, {marginLeft: 15}]}
							value={setInput.invoice_number}
							placeholder="Enter invoice here..."
							onChangeText={this.onChangeText("invoice_number")}
							returnKeyType='next'/>
						<TouchableOpacity onPress={() => navigation.navigate("QRScan")}>
							<Image style={styles.qrImage}
								source={Res.get("qr_dark")} resizeMode="contain"/>
						</TouchableOpacity>
					</View>

					<Text style={styles.txtLabel}>Take a photo of invoice</Text>
					<View style={[styles.dView2]}>
						{!_.isEmpty(setInput.image_url1) && <TouchableOpacity onPress={this.clearInvoice} style={styles.btnClear}>
							<Text style={styles.txtClear}>Clear</Text>
						</TouchableOpacity>}
						
						{_.isEmpty(setInput.image_url1) ?
							<TouchableOpacity onPress={this.openGalery}>
								<View elevation={5} style={[styles.itemContainer]}>
									<Image style={styles.addImage} source={Res.get("image_file")} />
									<Text style={styles.addText}>Add Invoice</Text>
								</View>
							</TouchableOpacity> :
							<TouchableOpacity onPress={() => this.onImageItemClick(setInput.image_url1)} style={[styles.itemContainer2]}>
								<Image style={styles.imageStyle} source={source} />
							</TouchableOpacity>}
					</View>

					<Text style={styles.txtLabel}>Payment details</Text>
					<PaymentInfo {...this.props} onChangeText={this.onChangeText} error={error}/>

					<Text style={styles.txtLabel}>Take a photo of boxes</Text>
					<View style={[styles.dView2]}>
						<View style={styles.imagesWrapper}>
							<Box TakeBoxes={() => this.TakeBoxes(2)} onViewImage={() => this.onImageItemClick(setInput.image_url2)}
								clearBox={() => this.clearBox("image_url2")} source={box1} style={style1}/>
							<Box TakeBoxes={() => this.TakeBoxes(3)} onViewImage={() => this.onImageItemClick(setInput.image_url3)}
								clearBox={() => this.clearBox("image_url3")} source={box2} style={style2}/>
							<Box TakeBoxes={() => this.TakeBoxes(4)} onViewImage={() => this.onImageItemClick(setInput.image_url4)}
								clearBox={() => this.clearBox("image_url4")} source={box3} style={style3}/>
						</View>
					</View>

					<Text style={styles.txtLabel}>Do you have promo code? </Text>
					<View style={[styles.dView3, styles.marB20]}>
						<TxtInput
							round
							value={setInput.promo_code}
							style={{width: 150}}
							style3={{height: 55}}
							inputStyles={{textAlign: "center"}}
							placeholder="xxxxxx"
							isClose={false}
							onChangeText={this.onChangeText("promo_code")}
							returnKeyType='next'/>
					</View>

					<FullImageView
						images={[{url: this.state.img || ""}]}
						visible={this.state.isFullView}
						onRequestClose={() => this.setState({isFullView: false})}/>

					<LoaderModal
						loading={isUploadingImage || imageLoad}
					/>
					<TransPass visible={isShowPin} value={setInput.PIN} error={error.PIN}
						onChangeText={this.onChangeText("PIN")}
						onSubmitEditing={() => this.onSubmit()}
						onCancel={() => this.setState({isShowPin: false})}
						onProceed={() => this.onSubmit()} isLoad={isSubmitViaOffline ? "Loading" : ""}/>
				</ScrollView>
			</SafeAreaView>
		);
	}
}

DetailScreen.propTypes = {
	home: PropTypes.object,
	login: PropTypes.object,
	actions: PropTypes.object,
	navigation: PropTypes.object,
};

export default DetailScreen;

