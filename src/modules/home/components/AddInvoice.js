/* eslint-disable react-native/no-inline-styles */

import React from "react";
import {View, FlatList, TouchableOpacity,
	SafeAreaView, Text, Alert} from "react-native";
import styles from "../styles.css";
import _ from "lodash";
import {Icon, ListItem} from "react-native-elements";
import PropTypes from "prop-types";
import Resource from "__src/resources";
import {HeaderBackButton} from "react-navigation";
import ClientInfo from "./screen/ClientInfo";
import Button from "__src/components/Button";
import UUIDV4 from "uuid/v4";
const {Color} = Resource;

class AddInvoice extends React.PureComponent{
	constructor(props){
		super(props);
		this.state = {
			isFullView: false, error: {},
			isShowPin: false, img: {},
		};
	}

	static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: (<HeaderBackButton tintColor="white"
				onPress={navigation.getParam("goback")}/>),
		};
	};

	componentDidMount(){
		const {navigation, actions, home: {invoiceCollection}} = this.props;

		if (_.isEmpty(invoiceCollection)){
			actions.setUUID(UUIDV4());
		}

		navigation.setParams({ goback: this.done });
	}

	done = () => {
		const {navigation, home: {invoiceCollection}} = this.props;

		if (_.isEmpty(invoiceCollection)){
			navigation.goBack();
			
			return;
		}

		Alert.alert("Notice", "Your data will be saved.",
			[{text: "Ok", onPress: () => this.goback()}]);
	}

	goback = () => {
		const {actions, navigation} = this.props;

		actions.resetCollection();
		actions.setInput({});
		navigation.goBack();
	}

	_renderItem = ({item, index}) => {
		return (
			<ListItem
				containerStyle={styles.containerStyle}
				key={`icx${index}`}
				title={`${item.invoice_number}`}
				titleStyle={styles.titleStyle}
			/>
		);
	}

	onChangeText = (type) => (value) => {
		const {home: {setInput}, actions} = this.props;
		const newInput = _.merge({}, setInput);
		const error = {};

		if (_.isEmpty(value)){
			error[type] = "This field is required.";
		}
		newInput[type] = value;
		this.setState({error});
		actions.setInput(newInput);
	}

	add = () => {
		const {navigation, home: {setInput}} = this.props;
		const error = {};

		if (_.isEmpty(setInput.firstname)){
			error.firstname = "This field is required.";
		}
		if (_.isEmpty(setInput.lastname)){
			error.lastname = "This field is required.";
		}

		this.setState({error});

		if (_.isEmpty(error)){
			navigation.navigate("AddDetail", {title: "Add Invoice"});
		}
	}

	render(){
		const {home: {invoiceCollection}} = this.props;
		const {error} = this.state;

		return (
			<SafeAreaView style={styles.container}>
				<View style={styles.flex1}>
					<ClientInfo {...this.props} onChangeText={this.onChangeText} error={error}/>
					<View style={[styles.flex1, styles.padH20]}>
						<View style={styles.view1}>
							<Text style={styles.txt3}>List of invoice</Text>
							<TouchableOpacity activeOpacity={0.7}
								style={styles.flexrow} onPress={this.add}>
								<Text style={styles.txtadd}>Add</Text>
								<Icon name="add" size={20} color={Color.colorPrimary}/>
							</TouchableOpacity>
						</View>
						<View style={[styles.flex1, styles.marginTop10]}>
							<FlatList
								data={invoiceCollection}
								extraData={this.state}
								keyExtractor={(item, idx) => `idx${idx}`}
								renderItem={this._renderItem}/>
							<Button
								onPress={this.done}
								labelStyle={{color: Color.colorPrimary, fontSize: 18}}
								style={[styles.style3, styles.marB20]}
								label="Done"
							/>
						</View>
					</View>
				</View>
			</SafeAreaView>
		);
	}
}

AddInvoice.propTypes = {
	home: PropTypes.object,
	login: PropTypes.object,
	actions: PropTypes.object,
	navigation: PropTypes.object,
};

export default AddInvoice;

