/* eslint-disable max-len */
import React, {PureComponent} from "react";
import {View, SafeAreaView, TouchableOpacity, Image,
	StatusBar, Alert, Text, Linking, Platform, PermissionsAndroid} from "react-native";
import {Tab, Tabs} from "native-base";
import Resource from "__src/resources";
import styles from "../styles.css";
import _ from "lodash";
import ForPickup from "./screen/ForPickup";
import ForPickupOffline from "./screen/ForPickupOffline";
import HomeItem from "./screen/HomeItem";
import HomeItemOffline from "./screen/HomeItemOffline";
import LoaderModal from "__src/components/LoaderModal";
import Loading from "__src/components/Loading";
import Account from "./screen/Account";
import PropTypes from "prop-types";
import Geolocation from "@react-native-community/geolocation";
import {Icon} from "react-native-elements";
import PINScreen from "../../login/components/PinScreen";
import {StackActions, NavigationActions} from "react-navigation";
import BackgroundTimer from "react-native-background-timer";
import NetInfo from "@react-native-community/netinfo";
import RNExitApp from "react-native-exit-app";

const {Color, Res} = Resource;
const arrays = ["OUTBOX", "SENT"];
const geoOptions = { enableHighAccuracy: true, timeout: 50000 };

export default class HomeTab extends PureComponent {
	constructor(props){
		super(props);
		this.state = {
			isVisible: false, search: "",
			switchpin: false,
			isConnected: false,
			isShowFilter: false,
		};
	}
  _subscription = null;
	
	static navigationOptions = ({ navigation }) => {
		const isLoad = _.has(navigation, "state.params.isLoad") ? navigation.state.params.isLoad : false;
		const onPress = isLoad ? navigation.getParam("onCheck") : navigation.getParam("calculator");

		return {
			headerTitle: "PICK-UP",
			headerLeft: (
				<TouchableOpacity activeOpacity={0.7} onPress={navigation.getParam("account")}>
					<Image style={styles.imgAccount}
						source={Res.get("user_icon")} resizeMode="contain" />
				</TouchableOpacity>),
			headerRight: (
				<TouchableOpacity activeOpacity={0.7} onPress={onPress}>
					{isLoad ? <Loading size="small" color="white"/> : <Text style={styles.txtLogout}>Calculator</Text>}
				</TouchableOpacity>),
		};
	};

	componentDidMount(){
		const {navigation} = this.props;

		NetInfo.fetch().then((state) => {
			this.setState({ isConnected: state.isConnected });
			if (state.isConnected){
				this.fetchForPickup();
			}
		});

		this._subscription = NetInfo.addEventListener((state) => {
			this.setState({ isConnected: state.isConnected });
		});
		navigation.setParams({ calculator: this.calculator, account: this.account,
			isLoad: this.state.isLoad, onCheck: this.onEdit  });
		
		this.getLocation();

		BackgroundTimer.runBackgroundTimer(() => {
			const {home: {listOfOffline, isReuploadPickup, setDuplicateData}, actions} = this.props;
			const {isConnected} = this.state;

			console.log("BackgroundTimer", isConnected, !_.isEmpty(listOfOffline), isReuploadPickup === false, _.isEmpty(setDuplicateData));
			if (!_.isEmpty(listOfOffline) && isConnected && isReuploadPickup === false && _.isEmpty(setDuplicateData)){
				console.log("submitDeliveryViaOnline", isConnected);
				navigation.setParams({ isLoad: true, onCheck: this.onEdit });
				actions.InvoiceReupload({...listOfOffline[0]});
			} else if (isReuploadPickup === false || !_.isEmpty(setDuplicateData)){
				navigation.setParams({ isLoad: false });
			}
		}, 6000);
	}

	getLocation = () => {
		return new Promise(async () => {
			if (Platform.OS === "android") {
				const permission = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

				if (permission === PermissionsAndroid.RESULTS.GRANTED) {
					Geolocation.getCurrentPosition(this.geoSuccess, this.geoFailure, geoOptions);
				} else {
					Alert.alert("Permission denied!", "Please allow your location in settings.",
						[{text: "OK", onPress: () => RNExitApp.exitApp()}],
						{cancelable: false});
				}
			} else {
				Geolocation.requestAuthorization();
				Geolocation.getCurrentPosition(this.geoSuccess, this.geoFailure, geoOptions);
			}
		});
	}

	componentDidUpdate(prevProps){
		const {home: {setDuplicateData, listOfOffline}} = this.props;

		if (!_.isEqual(prevProps.home.setDuplicateData, setDuplicateData) &&
			!_.isEmpty(setDuplicateData)){
			setTimeout(() => Alert.alert("Notice",
				`Invoice number (${setDuplicateData.invoiceno}) you are trying to add already exists. Please check the invoice or contact customer success team for further assistance.`,
				[{text: "Cancel", onPress: () => this.onEdit(setDuplicateData.params) },
					{text: "Save Anyway", onPress: () => this.onSubmitPending({...setDuplicateData.params, type: "edit"})} ], {cancelable: false}), 10);
		}

		if (!_.isEqual(prevProps.home.listOfOffline, listOfOffline) && _.isEmpty(listOfOffline)){
			this.fetchForPickup("noload");
		}
	}

	componentWillUnmount() {
		this._subscription && this._subscription();
		BackgroundTimer.stopBackgroundTimer();
		this._didFocusSubscription && this._didFocusSubscription.remove();
		this._willBlurSubscription && this._willBlurSubscription.remove();
	}

	onEdit = () => {
		const {home: {getTackingInvoice}} = this.props;

		Alert.alert("Uploading...",
			`Invoice ${getTackingInvoice} is being uploaded to the server`);
	}

	fetchForPickup = async(noload) => {
		const {actions, login: {session, setLoginCredential}} = this.props;
		const params = {
			login_id: session.result.user_id,
			agent_id: setLoginCredential.agent_id,
			order_column: 0,
			order_dir: "DESC",
			start: 0,
			length: 20,
			search_value: "",
		};

		actions.fetchOffline(params);
		actions.fetchForPickup(params, noload);
	}

	calculator = () => {
		Linking.openURL("https://apps.forexcargo.us/bds/calculator")
			.catch((err) => console.error("An error occurred", err));
	}

	geoSuccess = (position) => {
		const {actions} = this.props;

		console.log("geoSuccess", position);
		
		actions.saveLatLong({
		  latitude: position.coords.latitude,
		  longitude: position.coords.longitude,
		});
	};

	geoFailure = (err) => {
		console.log("geoFailure", err);
		// Alert.alert("Notice", "Unable to get location. Please enable your location in phone settings.");
	};

	account = () => {
		this.setState({isVisible: true});
	}

	logout = () => {
		Alert.alert("Notice", "Want to sign out?",
			[{
				text: "Cancel",
				onPress: () => console.log("Cancel Pressed"),
				style: "cancel",
			}, {text: "OK", onPress: () => this.onLogout()},
			], {cancelable: false});
	}

	onLogout = () => {
		const {actions, navigation} = this.props;
		const resetAction = StackActions.reset({
		  index: 0,
		  key: null,
		  actions: [NavigationActions.navigate({routeName: "Login"})],
		});

		actions.logout();
		navigation.dispatch(resetAction);
	}

	addInvoice = () => {
		const {actions, navigation} = this.props;

		actions.setInput({});
		actions.resetCollection();
		navigation.navigate("AddInvoice", {title: "Receive Boxes"});
	}

	onItemClick = (item) => {
		const {actions, navigation, login: {setLoginCredential}} = this.props;
		const params = {
			id: item.ID,
			agent_id: setLoginCredential.agent_id,
		};

		actions.getPickupDetails(params, navigation);
	}

	renderItem = ({item, index}) => {
		return (
			<HomeItem key={`${index}`} onPress={() => this.onItemClick(item)}
				item={item} {...this.props} />
		);
	}

	renderItemOffline = ({item, index}) => {
		return (
			<HomeItemOffline key={`${index}`}
				item={item} {...this.props}
				onPressPending={() => this.onSubmitPending(item)}
				onDelete={() => this.onEdit(item)}
				onReplace={() => this.onSubmitPending({...item, type: "edit"})} />
		);
	}

	onEdit = (item) => {
		const {actions, navigation} = this.props;

		actions.setInput(item);
		navigation.navigate("AddDetail", {title: "Add Invoice"});
		// actions.deleteRecord(item);
	}

	onSubmitPending = (params) => {
		const {actions} = this.props;

		if (this.state.isConnected){
			actions.ReplaceInvoice(params);
		} else {
			Alert.alert("Internet Required!", "Please check your internet connection.");
		}
	}

	onSwitch = () => {
		const {navigation} = this.props;

		this.setState({isVisible: false});

		const resetAction = StackActions.reset({
		  index: 0,
		  key: null,
		  actions: [NavigationActions.navigate({routeName: "LoginAs"})],
		});

		navigation.dispatch(resetAction);
	}

	onSeeMore = () => {
		const {actions, login: {session, setLoginCredential},
			home: {getForPickup}} = this.props;
		const params = {
			login_id: session.result.user_id,
			agent_id: setLoginCredential.agent_id,
			order_column: 0,
			order_dir: "DESC",
			start: getForPickup.length,
			length: 20,
			search_value: "",
		};

		actions.seeMorePickup(params);
	}

	renderPINScreen = () => (
		<PINScreen {...this.props} backToLogin={this.logout} />
	);

	onChangeSearchText = (value) => {
		this.setState({search: value.trim()});
	}

	onSubmitEditing = () => {
		if (!this.state.isConnected){
			Alert.alert("Network error!", "Please check your data connection.");
			
			return;
		}
		const {actions, login: {session, setLoginCredential}} = this.props;
		const {search} = this.state;
		const params = {
			login_id: session.result.user_id,
			agent_id: setLoginCredential.agent_id,
			order_column: 0,
			order_dir: "DESC",
			start: 0,
			length: 20,
			search_value: search,
		};

		actions.fetchForPickup(params);
	}
  
  renderTab = (item) => {
  	switch (item){
  	case "SENT":
  		return (<ForPickup key={item} tabLabel={item} {...this.props}
  			onPressFilter={() => this.setState({isShowFilter: !this.state.isShowFilter})}
  			isShowFilter={this.state.isShowFilter}search={this.state.search}
  			onChangeText={this.onChangeSearchText} onSubmitEditing={this.onSubmitEditing}
  			renderItem={this.renderItem} onSeeMore={this.onSeeMore} onRefresh={() => {
  				if (!this.state.isConnected){
  					Alert.alert("Network error!", "Please check your data connection.");
  					
  					return;
  				}
  				this.fetchForPickup();
  			}}/>);
  	case "OUTBOX":
  		return (
  			<ForPickupOffline key={item} tabLabel={item} {...this.props}
  				renderItem={this.renderItemOffline} />
  		);
  	}
  }

  render(){
  	const {home: {isGettingListForPickup, isGetPickupDetails},
  		login: {isRequiredPIN}} = this.props;
		
  	// if (isRequiredPIN){
  	// 	return this.renderPINScreen();
  	// }
		
  	return (
  		<SafeAreaView style={styles.flex1}>
  			<View style={styles.flex1}>
  				<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
  				<Tabs
  					tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
  					style={styles.TabsStyle}
  					tabBarActiveTextColor={Color.colorPrimary}
  					tabBarInactiveTextColor={Color.Standard2}>
  					{arrays.map((item, idx) => {
  						return (
  							<Tab key={`idx ${idx}`}
  								heading={`${item}`}
  								tabStyle={styles.tabStyle}
  								textStyle={styles.textStyle}
  								activeTabStyle={{backgroundColor: Color.white}}>
  								{this.renderTab(item)}
  							</Tab>
  						);
  					})}
  				</Tabs>
  				<TouchableOpacity activeOpacity={0.7} onPress={this.addInvoice}
  					style={styles.btnAdd}>
  					<Icon reverse size={30} name="add" color={Color.colorPrimary}/>
  				</TouchableOpacity>
  				<Account
  					{...this.props}
  					visible={this.state.isVisible}
  					onCancel={() => this.setState({isVisible: false})}
  					onSwitch={this.onSwitch}
  					onLogout={this.logout} />
  				<LoaderModal
  					loading={isGettingListForPickup || isGetPickupDetails} />
  			</View>
  		</SafeAreaView>
  	);
  }
}

HomeTab.propTypes = {
	actions: PropTypes.object,
	navigation: PropTypes.object,
	login: PropTypes.object,
	home: PropTypes.object,
};
