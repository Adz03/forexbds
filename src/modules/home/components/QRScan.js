/* eslint-disable */
import React, {PureComponent} from "react";
import {View, Text, ImageBackground, SafeAreaView, Animated,
	 Image, TouchableWithoutFeedback, StyleSheet} from "react-native";
import PropTypes from "prop-types";
import Resources from "__src/resources";
import { RNCamera } from "react-native-camera";
import _ from "lodash";
const {Res, Color} = Resources;

export default class QRScan extends PureComponent {
	constructor(props){
		super(props);
		this.state = {
			option: "",
			isBarcodeScannerEnabled: true
		}
	}

	static navigationOptions = {
		headerTitle: "Scan Code",
	}

	onBarCodeRead = (code) => {
		const {home: {setInput}, actions, navigation} = this.props;
		const {isBarcodeScannerEnabled} = this.state;
		if(isBarcodeScannerEnabled){
			const newInput = _.merge({}, setInput);

			newInput.invoice_number = code;
			actions.setInput(newInput);
			navigation.goBack();
			this.setState({isBarcodeScannerEnabled: false});
		}
	}

	renderCamera = () => {
		return (
			<View style={{height: "80%"}}>
				<RNCamera
					autoFocus={RNCamera.Constants.AutoFocus.on}
					onBarCodeRead={(e) => this.onBarCodeRead(e.data)} style={styles.camera2} />
			</View>
		)
	}

	renderQR = () => {
		return (
			<View style={{height: "80%"}}>
				<Text style={styles.txt1}>Place the code in the center of the square.</Text>
				<Text style={styles.txt2}>It will be scanned automatically.</Text>
				<View style={styles.txt3}>
					<RNCamera
						autoFocus={RNCamera.Constants.AutoFocus.on}
						onBarCodeRead={(e) => this.onBarCodeRead(e.data)} 
						style={styles.camera} />
				</View>
			</View>
		)
	}
  
	render() {

		return (
			<SafeAreaView style={[styles.container, {backgroundColor: Color.transparent}]}>
				<View style={styles.container}>
					{this.renderQR()}
					{/* <View style={styles.container2}>
						<TouchableWithoutFeedback
							onPress={() => this.onBarCodeRead("asfa")}
							onPressIn={this.handlePressIn}
							onPressOut={this.handlePressOut}>
							<Animated.View style={[styles.viewCameraStyle, animatedStyle]}>
								<Image style={styles.imageCameraStyle} source={Res.get("camera_active")} resizeMode="contain"/>
								<Text style={styles.txtCameraStyle}>Take a picture</Text>
							</Animated.View>
						</TouchableWithoutFeedback>
					</View> */}
				</View>
			</SafeAreaView>
		);
	}
}
QRScan.propTypes = {
	Title: PropTypes.string,
	placeholder: PropTypes.string,
	onBarCodeRead: PropTypes.func,
	onChangeText: PropTypes.func,
	onRequestClose: PropTypes.func,
	onPress: PropTypes.func,
	onPressOK: PropTypes.func,
	onPressCancel: PropTypes.func,
	value: PropTypes.string,
	visible: PropTypes.bool,
	isLoad: PropTypes.bool,
};
const styles = StyleSheet.create({
	container: {flex: 1, backgroundColor: "rgba(0,0,0,0.3)"},
	container2: {height: "20%", backgroundColor: Color.black, alignItems: "center", justifyContent: "center", paddingBottom: 9},
	camera: {width: 260, height: 260, alignItems: "center"},
	camera2: {width: "100%", height:  "100%"},
	txt1: {textAlign: "center", marginTop: 30, fontSize: 15, fontFamily: "Roboto", color: Color.white},
	txt2: {textAlign: "center", fontSize: 15, fontFamily: "Roboto", color: Color.white},
	txt3: {flex: 1, alignItems: "center", justifyContent: "center"},
	viewCameraStyle: {width: 200, height: 50, borderRadius: 25, flexDirection: "row", 
		backgroundColor: Color.gray05, justifyContent: "center", alignItems: "center"},
	txtCameraStyle: {flex: 1, fontFamily: "Roboto", fontSize: 18, color: Color.white, marginLeft: 5 },
	imageCameraStyle: {width: 50, height: 50},
	txtOR: {textAlign: "center", paddingTop: 10, fontSize: 16, fontFamily: "Roboto", color: Color.white},
});