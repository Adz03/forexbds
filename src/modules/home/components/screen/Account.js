import React from "react";
import {View, StyleSheet, Modal, TouchableWithoutFeedback, TouchableOpacity, Image} from "react-native";
import PropTypes from "prop-types";
import Detail from "__src/components/Detail";
import Resources from "__src/resources";
import Button from "__src/components/Button";
import {Icon} from "react-native-elements";
import _ from "lodash";
const {Color, Res} = Resources;

class Account extends React.PureComponent{
	render(){
		const {visible, onCancel, onSwitch, animate, onLogout, login: {session}} = this.props;
		const result = _.has(session, "result.first_name") ? session.result : {};
		
		return (
			<Modal animationType={animate} transparent visible={visible}
				onRequestClose={onCancel}>
				<TouchableWithoutFeedback onPress={onCancel}>
					<View style={styles.container}>
						<TouchableWithoutFeedback onPress={() => console.log("")}>
							<View style={styles.modalSubContainer1}>
								<TouchableOpacity onPress={onCancel} style={styles.btnClose}>
									<Icon name="close" size={35} color={Color.Header}/>
								</TouchableOpacity>
								<View style={[styles.containerLogo, styles.marT15]}>
									<Image	source={Res.get("user_icon")}
										style={styles.imageLogo}/>
								</View>
								<Detail horizontal label={"User ID:"} value={`${result.user_id}`} />
								<Detail horizontal label={"Full Name:"} value={`${result.first_name} ${result.last_name}`} />
								<Detail horizontal label={"Email:"} value={result.username} />
								<Detail horizontal label={"App version:"} value={"0.2.3"} />
								<Button onPress={onSwitch} label="Switch Account" style={styles.marT15}/>
								<Button onPress={onLogout} label="Logout"
									labelStyle={{color: Color.colorPrimary}}
									style={[styles.btnTrans, styles.marT5]}/>
							</View>
						</TouchableWithoutFeedback>
					</View>
				</TouchableWithoutFeedback>
			</Modal>
		);
	}
}

Account.propTypes = {
	visible: PropTypes.bool,
	animate: PropTypes.string,
	onCancel: PropTypes.func,
	onSwitch: PropTypes.func,
	onLogout: PropTypes.func,
	login: PropTypes.object,
};

const styles = StyleSheet.create({
	container: {flexShrink: 1, justifyContent: "center", width: "100%", height: "100%",
		paddingHorizontal: 25, paddingVertical: "10%", backgroundColor: "rgba(0, 0, 0, 0.5)"},
	modalSubContainer1: {flexShrink: 1, backgroundColor: "#fff", borderRadius: 10, padding: 20},
	containerLogo: { alignItems: "center", justifyContent: "flex-end", marginBottom: 10  },
	imageLogo: {width: 80, height: 80},
	marT15: {marginTop: 15},
	marT5: {marginTop: 5},
	btnClose: {position: "absolute", top: 7, right: 7},
	btnTrans: {backgroundColor: Color.transparent, borderBottomWidth: 0},
});

export default Account;
