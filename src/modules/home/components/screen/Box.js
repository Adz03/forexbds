import React from "react";
import {View, TouchableOpacity, Image, Dimensions} from "react-native";
import {Icon} from "react-native-elements";
import styles from "../../styles.css";
import PropTypes from "prop-types";
import Resource from "__src/resources";
const {Res} = Resource;
const {width} = Dimensions.get("window");
const numColumns = 3;

const Box = (props) => {
	const {source, clearBox, TakeBoxes, onViewImage, style} = props;
	const onPress = source ? onViewImage : TakeBoxes;

	return (
		<TouchableOpacity onPress={onPress}>
			<View elevation={5} style={[styles.itemContainer2,
				{width: (width - 80) / numColumns, height: (width - 80) / numColumns}]}>
				{source && <TouchableOpacity onPress={clearBox} style={styles.btnClear2}>
					<Icon name="close" color="red" size={25} />
				</TouchableOpacity>}
				<Image style={style} source={source || Res.get("box_img")}/>
			</View>
		</TouchableOpacity>
	);
};

Box.propTypes = {
	source: PropTypes.object,
	style: PropTypes.object,
	TakeBoxes: PropTypes.func,
	clearBox: PropTypes.func,
	onViewImage: PropTypes.func,
};

export default Box;
