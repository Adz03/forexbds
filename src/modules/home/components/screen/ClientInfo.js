import React from "react";
import {View, Text} from "react-native";
import TxtInput from "__src/components/TxtInput";
import styles from "../../styles.css";
import PropTypes from "prop-types";

class ClientInfo extends React.PureComponent{
	render(){
		const {home: {setInput}, onChangeText, error} = this.props;

		return (
			<View style={[styles.walletWrapper, styles.marB20]}>
				<Text style={[styles.txt3, styles.marB10]}>Recipient</Text>
				<TxtInput
					label="First Name"
					style={[styles.inputStyle]}
					value={setInput.firstname}
					placeholder="Ex. John"
					onChangeText={onChangeText("firstname")}
					err={error.firstname}
					returnKeyType='next'/>
				<TxtInput
					label="Last Name"
					style={[styles.inputStyle, styles.marginTop10]}
					value={setInput.lastname}
					err={error.lastname}
					placeholder="Ex. Carlos"
					onChangeText={onChangeText("lastname")}
					returnKeyType='next'/>
			</View>
		);
	}
}

ClientInfo.propTypes = {
	home: PropTypes.object,
	actions: PropTypes.object,
	onChangeText: PropTypes.func,
	error: PropTypes.object,
};

export default ClientInfo;
