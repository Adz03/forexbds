/* eslint-disable */
import React from "react";
import {FlatList, Text, View, TouchableOpacity} from "react-native";
import PropTypes from "prop-types";
import styles from "../../styles.css";
import {Icon} from "react-native-elements";
import TxtInput from "__src/components/TxtInput";
import Resource from "__src/resources";
const {Color} = Resource;

class ForPickup extends React.PureComponent{

	renderFilter = () => {
		const {search, onChangeText,onSubmitEditing} = this.props;

		return (
			<TxtInput
				round
				value={search}
				style={[styles.marB10]}
				returnKeyType="search"
				isClose={false}
				onSubmitEditing={onSubmitEditing}
				onBlur={onSubmitEditing}
				placeholder="What's your invoice?"
				onChangeText={onChangeText}/>
		)
	}

	render(){
		const {home: {getForPickup, totalPickup, isGettingListForPickup}, 
			renderItem, onSeeMore, onPressFilter, isShowFilter, onRefresh} = this.props;
		const length = getForPickup ? getForPickup.length : 0;

		return (
			<View style={[styles.flex1, styles.flatlist]}>
				<View style={[styles.viewHeader, styles.marginTop20]}>
					{/* <Text style={[styles.txtPickup]}>
						LIST OF PICKUP</Text> */}

					<TouchableOpacity
						onPress={onPressFilter}
						activeOpacity={0.7} style={styles.btnFilter}>
						<Icon name="tune" type="material-community" size={20} color={Color.colorPrimary}/>
						<Text style={{fontSize: 17, fontFamily: "Roboto", color: Color.colorPrimary}}>Filter</Text>
					</TouchableOpacity>
				</View>

				{isShowFilter && this.renderFilter()}

				<FlatList
					style={styles.flex1}
					showsVerticalScrollIndicator={false}
					data={getForPickup || []}
					refreshing={isGettingListForPickup}
					onRefresh={onRefresh}
					extraData={this.props}
					keyExtractor={(item, idx) => `idx${idx}`}
					ListFooterComponent={<View style={[styles.heigh90, styles.alignCenter]}>
						{length <  totalPickup &&
						<TouchableOpacity onPress={onSeeMore}>
							<Text style={styles.txtSeemore}>See more</Text>
						</TouchableOpacity>}
					</View>}
					ListEmptyComponent={<Text style={styles.txtNodata}>No data!</Text>}
					renderItem={renderItem}/>
			</View>
  		
  	);
	}
}

ForPickup.propTypes = {
	home: PropTypes.object,
	login: PropTypes.object,
	renderItem: PropTypes.func,
	onSeeMore: PropTypes.func,
	onChangeText: PropTypes.func,
	onSubmitEditing: PropTypes.func,
	isShowFilter: PropTypes.bool,
	search: PropTypes.string,
};

export default ForPickup;
