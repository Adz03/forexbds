/* eslint-disable */
import React from "react";
import {FlatList, Text, View} from "react-native";
import PropTypes from "prop-types";
import styles from "../../styles.css";
import _ from "lodash";

class ForPickupOffline extends React.PureComponent{
	render(){
  	const {home: {listOfOffline}, renderItem} = this.props;

  	return (
			<View style={[styles.flex1, styles.flatlist]}>
				<View style={[styles.viewHeader, styles.marginTop20]}>
					{/* <Text style={[styles.txtPickup]}>
					LIST OF PICKUP</Text> */}
				</View>
				<FlatList
					style={styles.flex1}
					showsVerticalScrollIndicator={false}
					data={listOfOffline || []}
					extraData={this.props}
					keyExtractor={(item, idx) => `idx${idx}`}
					ListEmptyComponent={<Text style={styles.txtNodata}>No data!</Text>}
					renderItem={renderItem}/>
			</View>
  	);
	}
}

ForPickupOffline.propTypes = {
	home: PropTypes.object,
	actions: PropTypes.object,
	login: PropTypes.object,
	isConnected: PropTypes.bool,
	renderItem: PropTypes.func,
	onItemOfflineClick: PropTypes.func,
};

export default ForPickupOffline;
