import React from "react";
import {Modal, StyleSheet, TouchableOpacity} from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import {Icon} from "react-native-elements";
import PropTypes from "prop-types";
import Loading from "__src/components/Loading";

export default class FullScreen extends React.PureComponent {
	render() {
		const {images, visible, onRequestClose} = this.props;

		return (
			<Modal visible={visible} transparent onRequestClose={onRequestClose}>
				<TouchableOpacity activeOpacity={0.7}
					onPress={onRequestClose} style={styles.btnClose}>
					<Icon type="material" name="highlight-off" color="white" size={40}/>
				</TouchableOpacity>
				<ImageViewer
					onSwipeDown={() => onRequestClose()}
					enableSwipeDown
					loadingRender={() => <Loading size="small" color="white" />}
					onCancel={onRequestClose} imageUrls={images}
					renderHeader={() => console.log()}
					renderIndicator={() => console.log()}/>
			</Modal>
		);
	}
}

FullScreen.propTypes = {
	images: PropTypes.oneOfType(PropTypes.array, PropTypes.object),
	visible: PropTypes.bool,
	onRequestClose: PropTypes.func,
};

const styles = StyleSheet.create({
	btnClose: {position: "absolute", top: 40, right: 10, zIndex: 1, elevation: 1},
});

