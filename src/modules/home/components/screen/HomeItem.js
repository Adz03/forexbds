/* eslint-disable react-native/no-inline-styles */
import React from "react";
import {View, Text, TouchableWithoutFeedback, Animated} from "react-native";
import {Card} from "native-base";
import PropTypes  from "prop-types";
import styles from "../../styles.css";
import Resources from "__src/resources";
import moment from "moment";
const {Color} = Resources;
const AnimatedCard = Animated.createAnimatedComponent(Card);

class HomeItem extends React.PureComponent{
	constructor(props){
		super(props);

		this.handlePressIn = this.handlePressIn.bind(this);
		this.handlePressOut = this.handlePressOut.bind(this);
	}
  animatePress = new Animated.Value(1);
		
  handlePressIn(){
  	Animated.spring(this.animatePress, {
  		toValue: 0.96,
  	}).start();
  }
		
  handlePressOut(){
  	Animated.spring(this.animatePress, {
  		toValue: 1,
  	}).start();
  }
  
  render(){
  	const {onPress, item} = this.props;
  	const animatedStyle = {
  		transform: [{ scale: this.animatePress}],
  	};
  	// const fullname = `${item.firstname} ${item.lastname}`;
  	const pickup = moment(item.PickupDate).format("MMM DD, YYYY");
    
  	return (
  		<TouchableWithoutFeedback
  			onPress={onPress}
  			onPressIn={this.handlePressIn}
  			onPressOut={this.handlePressOut}>
  			<AnimatedCard style={[styles.cardStyle, animatedStyle]}>
  				<View style={styles.viewStyle}>
  					<Text style={[styles.txt2, styles.marginTop5, {fontSize: 18}]}>
  						<Text style={[styles.txtBold]}>Invoice #:
  						</Text>{item.InvoiceNumber}</Text>
  					<Text style={[styles.txt1, styles.marginTop5]}>
  						<Text style={styles.txtBold}>Pickup Date: </Text>{pickup}</Text>
  					<Text style={[styles.txt1, styles.marginTop5]}>
  						<Text style={styles.txtBold}>Request by: </Text>{item.Customer_Name}</Text>
  				</View>
  				<View style={styles.viewStyle2}>
  					<View style={[styles.viewStyle3]}>
  						<Text style={{color: Color.white}}>{item.Status}</Text>
  					</View>
  					<View style={styles.viewStyle4}>
  						<Text style={{color: Color.Standard2}}>Total Fee</Text>
  						<Text style={styles.txtStyle}>{item.Total_Fee}</Text>
  					</View>
  				</View>
  			</AnimatedCard>
  		</TouchableWithoutFeedback>
  	);
  }
}

HomeItem.propTypes = {
	item: PropTypes.object,
	onPress: PropTypes.func,
};

export default HomeItem;
