/* eslint-disable max-len */
/* eslint-disable react-native/no-inline-styles */
import React from "react";
import {View, Text, Animated, TouchableOpacity} from "react-native";
import {Card} from "native-base";
import PropTypes  from "prop-types";
import styles from "../../styles.css";
import Resources from "__src/resources";
import Loading from "__src/components/Loading";
import _ from "lodash";
import moment from "moment";
const {Color} = Resources;
const AnimatedCard = Animated.createAnimatedComponent(Card);

class HomeItem extends React.PureComponent{
	constructor(props){
		super(props);

		this.handlePressIn = this.handlePressIn.bind(this);
		this.handlePressOut = this.handlePressOut.bind(this);
	}
  animatePress = new Animated.Value(1);
		
  handlePressIn(){
  	Animated.spring(this.animatePress, {
  		toValue: 0.96,
  	}).start();
  }
		
  handlePressOut(){
  	Animated.spring(this.animatePress, {
  		toValue: 1,
  	}).start();
  }
	
	renderStatus = (item) => {
		const { home: {getTackingInvoice, isReuploadPickup, setDuplicateData, isReplacingInvoice}, onPressPending} = this.props;
		const isLoad = item.invoice_number === getTackingInvoice && isReuploadPickup && _.isEmpty(setDuplicateData);
		const isLoad2 = item.invoice_number === getTackingInvoice && isReplacingInvoice;
		const error = item.invoice_number === setDuplicateData.invoiceno ? "Error" : "Sync now";
		const onPress = item.invoice_number === setDuplicateData.invoiceno ? null : onPressPending;

		if (isLoad || isLoad2){
			return (
				<View style={[styles.viewStyle3,
					{backgroundColor: Color.red}]}>
					<Loading size="small" color="white"/>
				</View>
			);
		}
		
		return (
			<TouchableOpacity onPress={onPress}
				activeOpacity={0.6}  style={[styles.viewStyle3,
					{backgroundColor: Color.red}]}>
				<Text style={{color: Color.white}}>{error}</Text>
			</TouchableOpacity>
		);
	}

	renderButton = (item) => {
		const { home: {setDuplicateData}, onReplace, onDelete} = this.props;

		if (item.invoice_number === setDuplicateData.invoiceno){
			return (
				<View style={{marginTop: 10}}>
					<Text style={{fontFamily: "Roboto", fontSize: 14, color: Color.Header}}>Note: This invoice is already exist. Please check the invoice or contact customer success team for further assistance.</Text>
					<View style={{flexDirection: "row", justifyContent: "center", marginTop: 7}}>
						<TouchableOpacity onPress={onDelete} style={{paddingHorizontal: 20, paddingVertical: 5, backgroundColor: Color.lightgreen, borderRadius: 5, marginRight: 7}}>
							<Text style={{fontFamily: "Roboto", fontSize: 15, color: "white", textAlign: "center", fontWeight: "bold"}}>Edit</Text>
						</TouchableOpacity>

						<TouchableOpacity onPress={onReplace} style={{paddingHorizontal: 10, paddingVertical: 5,  backgroundColor: Color.lightgreen, borderRadius: 5, marginLeft: 7 }}>
							<Text style={{fontFamily: "Roboto", fontSize: 15, color: "white", textAlign: "center", fontWeight: "bold"}}>Save Anyway</Text>
						</TouchableOpacity>
					</View>
				</View>
				
			);
		}

		return null;
	}
  
	render(){
  	const {onPress, item, onDelete, onReplace} = this.props;
  	const animatedStyle = {
  		transform: [{ scale: this.animatePress}],
  	};
  	const fullname = `${item.firstname} ${item.lastname}`;
  	const pickup = moment(item.PickupDate).format("MMM DD, YYYY");
  	const Total_Fee = item.payment_check + item.payment_cash + item.payment_cc + item.pickup_fee;
  	// const isLoad = item.invoice_number === getTackingInvoice && isReuploadPickup;
    
  	return (
  		<View
  			onPress={onPress}
  			onPressIn={this.handlePressIn}
  			onPressOut={this.handlePressOut}>
  			<AnimatedCard style={[styles.cardStyle2, animatedStyle]}>
  				<View style={{flexDirection: "row" }}>
  					<View style={styles.viewStyle}>
  						<Text style={[styles.txt2, styles.marginTop5, {fontSize: 18}]}>
  							<Text style={[styles.txtBold]}>Invoice #:
  							</Text>{item.invoice_number}</Text>
  						<Text style={[styles.txt1, styles.marginTop5]}>
  							<Text style={styles.txtBold}>Pickup Date: </Text>{pickup}</Text>
  						<Text style={[styles.txt1, styles.marginTop5]}>
  							<Text style={styles.txtBold}>Request by: </Text>{fullname}</Text>
  					</View>
  					<View style={styles.viewStyle2}>
  						{this.renderStatus(item)}
  						<View style={styles.viewStyle4}>
  							<Text style={{color: Color.Standard2}}>Total Fee</Text>
  							<Text style={styles.txtStyle}>{Total_Fee}</Text>
  						</View>
  					</View>
  				</View>
  				{this.renderButton(item)}
  			</AnimatedCard>
  		</View>
  	);
	}
}

HomeItem.propTypes = {
	item: PropTypes.object,
	onPress: PropTypes.func,
	onPressPending: PropTypes.func,
	onDelete: PropTypes.func,
	onReplace: PropTypes.func,
	home: PropTypes.object,
};

export default HomeItem;
