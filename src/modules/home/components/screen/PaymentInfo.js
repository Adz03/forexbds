import React from "react";
import {View} from "react-native";
import TxtInput from "__src/components/TxtInput";
import styles from "../../styles.css";
import PropTypes from "prop-types";
import _ from "lodash";

class PaymentInfo extends React.PureComponent{

	isNumber = (item) => {
		if (_.isNull(item)){
			return "";
		} else if (_.isNumber(item)){
			return `${item}`;
		}
		
		return item;
	}

	render(){
		const {home: {setInput}, onChangeText, error, isText} = this.props;
		const border0 = isText ? {borderBottomWidth: 0} : null;

		return (
			<View style={[styles.dView1]}>
				<TxtInput
					label="Cash"
					isText={isText}
					style={[styles.inputStyle]}
					value={this.isNumber(setInput.payment_cash)}
					keyboardType="decimal-pad"
					err={error.payment_cash}
					style3={border0}
					placeholder="0.00"
					onChangeText={onChangeText("payment_cash")}
					returnKeyType='next'/>
				<TxtInput
					label="Credit"
					isText={isText}
					style3={border0}
					style={[styles.inputStyle, styles.marginTop10]}
					value={this.isNumber(setInput.payment_cc)}
					keyboardType="decimal-pad"
					placeholder="0.00"
					err={error.payment_cc}
					onChangeText={onChangeText("payment_cc")}
					returnKeyType='next'/>
				<TxtInput
					label="Check"
					isText={isText}
					style3={border0}
					style={[styles.inputStyle, styles.marginTop10]}
					value={this.isNumber(setInput.paymen_check)}
					err={error.paymen_check}
					placeholder="0.00"
					keyboardType="decimal-pad"
					onChangeText={onChangeText("paymen_check")}
					returnKeyType='next'/>
				<TxtInput
					label="Pickup Fee (Optional)"
					style3={border0}
					style={[styles.inputStyle, styles.marginTop10]}
					isText={isText}
					value={this.isNumber(setInput.pickup_fee)}
					err={error.pickup_fee}
					placeholder="0.00"
					keyboardType="decimal-pad"
					onChangeText={onChangeText("pickup_fee")}
					returnKeyType='next'/>
			</View>
		);
	}
}

PaymentInfo.propTypes = {
	home: PropTypes.object,
	isText: PropTypes.bool,
	error: PropTypes.object,
	actions: PropTypes.object,
	onChangeText: PropTypes.func,
};

export default PaymentInfo;
