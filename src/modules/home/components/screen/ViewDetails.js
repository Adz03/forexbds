/* eslint-disable no-inline-comments */
/* eslint-disable line-comment-position */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable max-len */
import React from "react";
import {View, ScrollView, TouchableOpacity, Image,
	SafeAreaView, Text, Dimensions, TextInput} from "react-native";
import styles from "../../styles.css";
import {Icon} from "react-native-elements";
import PropTypes from "prop-types";
import TxtInput from "__src/components/TxtInput";
import FullImageView from "./FullImageView";
import PaymentInfo from "./PaymentInfo";
import Box from "./Box";
const {width} = Dimensions.get("window");
const numColumns = 3;

class ViewDetails extends React.PureComponent{
	constructor(props){
		super(props);
		this.state = {
			isFullView: false,
			error: {}, isShowPin: false,
			isConnected: false, img: {},
		};
	}

	onImageItemClick = (img) => {
		this.setState({isFullView: true, img});
	}

	renderItem = ({item, index}) => {
		if (item.empty === true) {
			return (<View key={`idx${index}`}
				style={[styles.itemContainer, styles.itemInvisible]} />);
		}
							
		return (
			<TouchableOpacity activeOpacity={0.7} key={`${index}`} onPress={() => this.onImageItemClick(item)}>
				<View elevation={5} style={[styles.itemContainer,
					{width: (width - 80) / numColumns, height: (width - 80) / numColumns}]}>
					<TouchableOpacity onPress={this.openGalery} style={styles.btnClose}>
						<Icon name="highlight-off" size={22} color="red"/>
					</TouchableOpacity>
					<Image style={styles.imageStyle} source={item.source} resizeMode="contain" />
				</View>
			</TouchableOpacity>
		);
	}

	onChangeText = () => () => {
		console.log();
	}

	render(){
		const {home: {setInput}} = this.props;
		const {error} = this.state;
		const source = setInput.image_url1 ? {uri: setInput.image_url1} : null;
		const box1 = setInput.image_url2 ? {uri: setInput.image_url2} : null;
		const box2 = setInput.image_url3 ? {uri: setInput.image_url3} : null;
		const box3 = setInput.image_url4 ? {uri: setInput.image_url4} : null;
		const style1 = setInput.image_url2 ? styles.imageStyle : styles.addImage;
		const style2 = setInput.image_url3 ? styles.imageStyle : styles.addImage;
		const style3 = setInput.image_url4 ? styles.imageStyle : styles.addImage;

		return (
			<SafeAreaView style={[styles.container, {backgroundColor: "white"}]}>
				<ScrollView showsVerticalScrollIndicator={false} style={[styles.container, {backgroundColor: "white"}]}>
					<Text style={styles.txtLabel}>Invoice Number</Text>
					<View style={[styles.dView0, styles.marginTop5]}>
						<TextInput style={[styles.textfields, {marginLeft: 15}]}
							value={setInput.invoice_number}
							placeholder="Enter invoice here..."
							editable={false}
							returnKeyType='next'/>
					</View>

					<Text style={styles.txtLabel}>Take a photo of invoice</Text>
					<View style={[styles.dView2]}>
						<TouchableOpacity onPress={() => this.onImageItemClick(setInput.image_url1)} style={[styles.itemContainer2]}>
							<Image style={styles.imageStyle} source={source} />
						</TouchableOpacity>
					</View>

					<Text style={styles.txtLabel}>Payment details</Text>
					<PaymentInfo isText {...this.props} onChangeText={this.onChangeText} error={error}/>

					<Text style={styles.txtLabel}>Take a photo of boxes</Text>
					<View style={[styles.dView2]}>
						<View style={styles.imagesWrapper}>
							{box1 && <Box source={box1} style={style1} onViewImage={() => this.onImageItemClick(setInput.image_url2)}/>}
							{box2 && <Box source={box2} style={style2} onViewImage={() => this.onImageItemClick(setInput.image_url3)}/>}
							{box3 && <Box source={box3} style={style3} onViewImage={() => this.onImageItemClick(setInput.image_url4)}/>}
						</View>
					</View>

					<Text style={styles.txtLabel}>Do you have promo code? </Text>
					<View style={[styles.dView3, styles.marB20]}>
						<TxtInput
							round
							editable={false}
							value={setInput.promo_code}
							style={{width: 150}}
							style3={{height: 55}}
							inputStyles={{textAlign: "center"}}
							isClose={false}
							isText
							returnKeyType='next'/>
					</View>

					<FullImageView
						images={[{url: this.state.img || ""}]}
						visible={this.state.isFullView}
						onRequestClose={() => this.setState({isFullView: false})}/>
				</ScrollView>
			</SafeAreaView>
		);
	}
}

ViewDetails.propTypes = {
	home: PropTypes.object,
	login: PropTypes.object,
	actions: PropTypes.object,
	navigation: PropTypes.object,
};

export default ViewDetails;

