import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";

import AddInvoice from "../components/AddInvoice";

const mapStateToProps = ({ login, home }) => ({
	login,
	home,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({...ActionCreators}, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddInvoice);
