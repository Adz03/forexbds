import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import * as ActionCreatorsLogin from "../../login/actions";

import HomeScreen from "../components/HomeTab";

const mapStateToProps = ({ login, home }) => ({
	login,
	home,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({...ActionCreators, ...ActionCreatorsLogin}, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
