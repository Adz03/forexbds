import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";

import ViewDetails from "../components/screen/ViewDetails";

const mapStateToProps = ({ login, home }) => ({
	login,
	home,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({...ActionCreators}, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewDetails);
