import Home from "./containers/Home";
import QRScan from "./containers/QRScan";
import AddDetail from "./containers/Detail";
import ViewDetails from "./containers/ViewDetail";
import AddInvoice from "./containers/AddInvoice";
import navigationOptions from "__src/components/navOpt";

import reducers from "./reducers";

export const home = reducers;
export default {
	ViewDetails: {
		screen: ViewDetails,
		navigationOptions,
	},
	AddDetail: {
		screen: AddDetail,
		navigationOptions,
	},
	AddInvoice: {
		screen: AddInvoice,
		navigationOptions,
	},
	QRScan: {
		screen: QRScan,
		navigationOptions,
	},
	Home: {
		screen: Home,
		navigationOptions,
	},
};
