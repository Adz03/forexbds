import { combineReducers } from "redux";
import _ from "lodash";
import * as Types from "./types";

const isUploadingImage = (state = false, action) => {
	switch (action.type){
	case Types.UPLOAD_IMAGE_LOAD:
		return true;
	case Types.ADD_IMAGE:
	case Types.UPLOAD_IMAGE_FAILED:
	case Types.UPLOAD_IMAGE:
		return false;
	default:
		return state;
	}
};

const imageLoad = (state = false, action) => {
	switch (action.type){
	case Types.IMAGE_LOADING:
		return action.data;
	case Types.LOGOUT:
		return false;
	default:
		return state;
	}
};

const setInput = (state = {}, action) => {
	switch (action.type){
	case Types.SET_INPUT:
		return action.data;
	default:
		return state;
	}
};

const invoiceImage = (state = {}, action) => {
	switch (action.type){
	case Types.UPLOAD_IMAGE:
		return action.data;
	default:
		return state;
	}
};

const getAllImageUpload = (state = [], action) => {
	switch (action.type){
	case Types.ADD_IMAGE:
		const newState = [...state];

		newState.push(action.data);
		
		return newState;
	case Types.RESET_IMAGE:
		return [];
	default:
		return state;
	}
};

const isForPickupLoad = (state = false, action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP_PROGRESS:
		return true;
	case Types.INPUT_FORPICKUP:
	case Types.INPUT_FORPICKUP_FAILED:
	case Types.INPUT_FORPICKUP_OFFLINE:
		return false;
	default:
		return state;
	}
};

const inputPickupFailed = (state = "", action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP_FAILED:
		return action.error;
	case Types.INPUT_FORPICKUP:
	case Types.INPUT_FORPICKUP_PROGRESS:
		return "";
	default:
		return state;
	}
};

const isSubmitViaOffline = (state = false, action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP_OFFLINE_PROGRESS:
		return true;
	case Types.INPUT_FORPICKUP_OFFLINE:
	case Types.INPUT_FORPICKUP_OFFLINE_FAILED:
		return false;
	default:
		return state;
	}
};

const listOfOffline = (state = [], action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP_OFFLINE:
		return action.data;
	default:
		return state;
	}
};

const submitViaOfflineFailed = (state = "", action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP_OFFLINE_FAILED:
		return action.error;
	case Types.INPUT_FORPICKUP_OFFLINE:
	case Types.INPUT_FORPICKUP_OFFLINE_PROGRESS:
		return "";
	default:
		return state;
	}
};

const listOfPickup = (state = [], action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP:
		return action.data;
	case Types.INPUT_FORPICKUP_FAILED:
	case Types.INPUT_FORPICKUP_PROGRESS:
		return [];
	default:
		return state;
	}
};

const invoiceCollection = (state = [], action) => {
	switch (action.type){
	case Types.INPUT_FORPICKUP:
		const newState = [...state];

		newState.push(action.data);
		
		return newState;
	case Types.RESET_FORPICKUP:
		return [];
	default:
		return state;
	}
};

const getTackingInvoice = (state = "", action) => {
	switch (action.type){
	case Types.SET_TRACKING_INVOICE:
		return action.data;
	default:
		return state;
	}
};
const isReuploadPickup = (state = false, action) => {
	switch (action.type){
	case Types.REUPLOAD_INVOICE_PROGRESS:
		return true;
	case Types.REUPLOAD_INVOICE:
	case Types.REUPLOAD_INVOICE_FAILED:
	case Types.SET_REQUIRED_PIN:
	case Types.DELETE_RECORD_FROM_SQL:
	case Types.INPUT_FORPICKUP_OFFLINE:
		return false;
	default:
		return state;
	}
};

const reuploadPickup = (state = [], action) => {
	switch (action.type){
	case Types.REUPLOAD_INVOICE:
		return action.data;
	case Types.REUPLOAD_INVOICE_FAILED:
	case Types.REUPLOAD_INVOICE_PROGRESS:
		return [];
	default:
		return state;
	}
};

const reuploadFailed = (state = "", action) => {
	switch (action.type){
	case Types.REUPLOAD_INVOICE_FAILED:
		return action.error;
	case Types.REUPLOAD_INVOICE:
	case Types.REUPLOAD_INVOICE_PROGRESS:
		return "";
	default:
		return state;
	}
};

const isGettingListForPickup = (state = false, action) => {
	switch (action.type){
	case Types.GET_FORPICKUP_PROGRESS:
		return true;
	case Types.GET_FORPICKUP:
	case Types.GET_FORPICKUP_FAILED:
	case Types.SEE_MORE_PICKUP:
		return false;
	default:
		return state;
	}
};

const getForPickup = (state = [], action) => {
	switch (action.type){
	case Types.GET_FORPICKUP:
		return action.data;
	case Types.SEE_MORE_PICKUP:
		const newState = [...state];

		return _.concat(newState, action.data);
	case Types.RESET_PICKUP:
		return [];
	default:
		return state;
	}
};

const totalPickup = (state = 0, action) => {
	switch (action.type){
	case Types.GET_COUNT_PICKUP:
		return action.data;
	case Types.RESET_PICKUP:
		return 0;
	default:
		return state;
	}
};

const saveLatLong = (state = {}, action) => {
	switch (action.type) {
	case Types.SAVE_LAT_LONG:
		return action.data;
	default:
		return state;
	}
};

const isGetPickupDetails = (state = false, action) => {
	switch (action.type) {
	case Types.GET_PICKUP_DETAILS_PROGRESS:
		return true;
	case Types.SET_INPUT:
	case Types.GET_PICKUP_DETAILS:
	case Types.GET_PICKUP_DETAILS_FAILED:
		return false;
	default:
		return state;
	}
};

const setUUID = (state = "", action) => {
	switch (action.type) {
	case Types.SET_UNIQUE_UUID:
		return action.data;
	default:
		return state;
	}
};

const setDuplicateData = (state = {}, action) => {
	switch (action.type) {
	case Types.SET_DATA_FOR_DUPLICATE:
		return action.data;
	case Types.REUPLOAD_INVOICE:
	case Types.DELETE_RECORD_FROM_SQL:
	case Types.INPUT_FORPICKUP_OFFLINE:
		return {};
	default:
		return state;
	}
};

const isReplacingInvoice = (state = false, action) => {
	switch (action.type) {
	case Types.REPLACE_INVOICE_PROGRESS:
		return true;
	case Types.REPLACE_INVOICE_FAILED:
	case Types.REPLACE_INVOICE:
		return false;
	default:
		return state;
	}
};

const replaceSuccess = (state = {}, action) => {
	switch (action.type) {
	case Types.REPLACE_INVOICE:
		return action.data;
	case Types.REPLACE_INVOICE_FAILED:
	case Types.REPLACE_INVOICE_PROGRESS:
		return {};
	default:
		return state;
	}
};

const setCamera = (state = false, action) => {
	switch (action.type) {
	case Types.SET_CAMERA:
		return action.data;
	case Types.LOGOUT:
		return false;
	default:
		return state;
	}
};

export default combineReducers({
	isUploadingImage,
	setInput,
	getAllImageUpload,
	invoiceImage,

	isSubmitViaOffline,
	listOfOffline,
	submitViaOfflineFailed,

	isForPickupLoad,
	isGettingListForPickup,
	getForPickup,
	totalPickup,
	listOfPickup,

	inputPickupFailed,
	reuploadPickup,
	reuploadFailed,
	isReuploadPickup,
	saveLatLong,
	invoiceCollection,
	isGetPickupDetails,
	setUUID,
	getTackingInvoice,

	imageLoad,
	setDuplicateData,

	isReplacingInvoice,
	replaceSuccess,
	setCamera,
});

