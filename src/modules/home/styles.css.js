import { StyleSheet } from "react-native";
import Color from "__src/resources/styles/color";

export default StyleSheet.create({
	container: { flexShrink: 1, width: "100%",
		height: "100%", backgroundColor: Color.bg },
	body: {flex: 1, backgroundColor: Color.white },

	// Home
	txt1: { fontFamily: "Roboto-Light", fontSize: 15, color: Color.Standard2 },
	txt2: { fontFamily: "Roboto-Light", fontSize: 17, color: Color.Standard2, fontWeight: "bold" },
	txtBold: { fontFamily: "FontAwesome", fontWeight: "bold" },
	flex1: { flex: 1, backgroundColor: Color.bg },
	listItemContainer: { padding: 15 },
	padH20marT20: { paddingHorizontal: 20, marginTop: 20 },
	listItemSignal: { width: 30, height: 30, borderRadius: 15,
		borderWidth: 1, borderColor: Color.Standard2, backgroundColor: Color.green },
	marginTop5: { marginTop: 5 },
	marginTop7: { marginTop: 7 },
	marginTop10: { marginTop: 10 },
	marginTop20: { marginTop: 20 },
	marB10: {marginBottom: 10},
	marB20: {marginBottom: 20},
	marV20: {marginVertical: 20},
	marH20: {marginHorizontal: 20},
	padH10: {paddingHorizontal: 10},
	padH20: {paddingHorizontal: 20},
	gradient: { position: "absolute", left: 0, bottom: 0,
		right: 0, alignItems: "center"},
	txtPickup: {fontFamily: "Roboto", fontSize: 18, fontWeight: "700", color: Color.black},
	txtLogout: {fontFamily: "Roboto-Light", fontSize: 18, fontWeight: "700", color: Color.white, marginRight: 6},
	txtNodata: {fontFamily: "Roboto-Light", fontSize: 16, textAlign: "center", color: Color.Standard2},
	heigh90: {height: 90},
	alignCenter: {alignItems: "center", justifyContent: "center"},
	txtSeemore: {fontFamily: "Roboto", color: Color.Standard2, fontSize: 16},
	tabBarUnderlineStyle: {height: 1, backgroundColor: Color.colorPrimary},
	tabStyle: {backgroundColor: Color.white},
	TabsStyle: {backgroundColor: Color.white, alignItems: "center", justifyContent: "center"},
	textStyle: {color: Color.Standard2, fontFamily: "Roboto-Light", fontSize: 12},
	imgAccount: {width: 30, height: 30, marginLeft: 6},
	btnAdd: {position: "absolute", bottom: 6, right: 6},
	flatlist: {paddingHorizontal: 15},
	viewHeader: {flexDirection: "row", alignItems: "center", justifyContent: "flex-end", marginVertical: 10},
	
	// Details screen
	dView0: { minHeight: 50, marginHorizontal: 20, backgroundColor: Color.white, flexDirection: "row",
		borderRadius: 6, padding: 3, justifyContent: "center", alignItems: "center", borderWidth: 0.5, borderColor: Color.Standard2	},
	dView1: { marginHorizontal: 20, backgroundColor: Color.white,
		borderRadius: 6, flexWrap: "wrap", paddingVertical: 20	},
	dView2: {flexShrink: 1, marginHorizontal: 20, backgroundColor: Color.white,
		borderRadius: 6, padding: 3, alignItems: "center", justifyContent: "center"},
	dView3: {flexShrink: 1, minHeight: 100, marginHorizontal: 20, backgroundColor: Color.white,
		borderRadius: 6, padding: 3, justifyContent: "center", alignItems: "center"},
	itemContainer: { margin: 5,	padding: 20, height: 140, width: 140,
		alignItems: "center", justifyContent: "center", flexDirection: "column",
		borderRadius: 5, borderColor: Color.LightDark, borderStyle: "dashed",
		borderWidth: StyleSheet.hairlineWidth },
	itemContainer2: { padding: 5, height: 150, width: 150,
		borderRadius: 5, borderColor: Color.LightDark, borderStyle: "dashed",
		borderWidth: StyleSheet.hairlineWidth, justifyContent: "center" },
	itemInvisible: {backgroundColor: "transparent", borderWidth: 0},
	txtLabel: {paddingHorizontal: 20, fontSize: 18,
		color: Color.Header, marginTop: 20, marginBottom: 10},
	btnClose: {position: "absolute", top: -5, right: -5},
	imageStyle: {flex: 1, width: "100%", height: "100%", borderRadius: 5},
	imagesWrapper: {flexDirection: "row", flexWrap: "wrap", flex: 1, marginTop: 10},
	addImage: {width: 55, height: 55, alignSelf: "center"},
	addText: {fontFamily: "Roboto-Light", fontSize: 15, marginTop: 5, fontWeight: "600"},
	inputStyle: {width: "100%"},
	textfields: { marginLeft: 5, flex: 1,  fontSize: 16, fontFamily: "Roboto-Light", fontWeight: "700" },
	qrImage: {width: 40, height: 40, padding: 5, marginRight: 5},
	btnlogin: {height: 50, borderBottomWidth: 6, borderRadius: 3},
	style2: {height: 50, borderBottomWidth: 6, borderRadius: 3, width: "100%"},
	style3: {backgroundColor: "transparent", borderBottomWidth: 0},
	btnClear: {position: "absolute", top: 10, right: 10, zIndex: 1, elevation: 1},
	btnClear2: {position: "absolute", top: -4, right: -4, zIndex: 1, elevation: 1},
	txtClear: {fontFamily: "Roboto", fontSize: 15, color: Color.Standard2},
	
	// HOME ITEM
	cardStyle: {flexDirection: "row", marginTop: 10, borderRadius: 10, padding: 15},
	cardStyle2: {marginTop: 10, borderRadius: 10, padding: 15},
	viewStyle: {width: "65%", alignItems: "flex-start"},
	viewStyle2: {width: "35%", justifyContent: "center", alignItems: "center"},
	viewStyle3: {backgroundColor: Color.lightgreen, justifyContent: "center", alignItems: "center",
		height: 30, width: "100%", borderRadius: 3},
	viewStyle4: {borderWidth: 0.5, borderColor: Color.Standard2, height: 60,
		width: "100%", borderRadius: 3, marginTop: 5, justifyContent: "center", alignItems: "center"},
	txtStyle: {color: Color.Standard2, fontSize: 17, marginTop: 3},
	btnFilter: {flexDirection: "row", alignItems: "center", justifyContent: "center"},

	// CLIENT INFO
	walletWrapper: { backgroundColor: Color.gray06,
		padding: 20},

	// ADD INVOICE
	containerStyle: {paddingVertical: 15, backgroundColor: "transparent", paddingLeft: 0},
	titleStyle: {fontFamily: "Roboto-Light", fontSize: 17},
	view1: {flexDirection: "row", alignItems: "center", justifyContent: "space-between"},
	txt3: {fontSize: 18, color: Color.Standard2, fontWeight: "600"},
	txtadd: {fontFamily: "Roboto-Light", fontSize: 15, color: Color.colorPrimary},
	flexrow: {flexDirection: "row"},
});
