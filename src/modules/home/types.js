
export const LOGIN_USER_OUTLET_NOTACTIVE = "login/types/LOGIN_USER_OUTLET_NOTACTIVE";
export const LOGOUT = "home/types/LOGOUT";
export const SET_LOGIN_DETAILS = "login/types/SET_LOGIN_DETAILS";

export const UPLOAD_IMAGE_LOAD = "home/types/UPLOAD_IMAGE_LOAD";
export const UPLOAD_IMAGE = "home/types/UPLOAD_IMAGE";
export const UPLOAD_IMAGE_FAILED = "home/types/UPLOAD_IMAGE_FAILED";

export const GET_LOCATION_PROGRESS = "home/types/GET_LOCATION_PROGRESS";
export const GET_LOCATION = "home/types/GET_LOCATION";
export const GET_LOCATION_FAILED = "home/types/GET_LOCATION_FAILED";

export const INPUT_FORPICKUP_PROGRESS = "home/types/INPUT_FORPICKUP_PROGRESS";
export const INPUT_FORPICKUP = "home/types/INPUT_FORPICKUP";
export const INPUT_FORPICKUP_FAILED = "home/types/INPUT_FORPICKUP_FAILED";

export const INPUT_FORPICKUP_OFFLINE_PROGRESS = "home/types/INPUT_FORPICKUP_OFFLINE_PROGRESS";
export const INPUT_FORPICKUP_OFFLINE = "home/types/INPUT_FORPICKUP_OFFLINE";
export const INPUT_FORPICKUP_OFFLINE_FAILED = "home/types/INPUT_FORPICKUP_OFFLINE_FAILED";

export const SET_REQUIRED_PIN = "login/types/SET_REQUIRED_PIN";

export const REPLACE_INVOICE_PROGRESS = "home/types/REPLACE_INVOICE_PROGRESS";
export const REPLACE_INVOICE = "home/types/REPLACE_INVOICE";
export const REPLACE_INVOICE_FAILED = "home/types/REPLACE_INVOICE_FAILED";

export const REUPLOAD_INVOICE_PROGRESS = "home/types/REUPLOAD_INVOICE_PROGRESS";
export const REUPLOAD_INVOICE = "home/types/REUPLOAD_INVOICE";
export const REUPLOAD_INVOICE_FAILED = "home/types/REUPLOAD_INVOICE_FAILED";
export const SET_TRACKING_INVOICE = "home/types/SET_TRACKING_INVOICE";

export const GET_FORPICKUP_PROGRESS = "home/types/GET_FORPICKUP_PROGRESS";
export const GET_FORPICKUP = "home/types/GET_FORPICKUP";
export const GET_FORPICKUP_FAILED = "home/types/GET_FORPICKUP_FAILED";
export const SEE_MORE_PICKUP = "home/types/SEE_MORE_PICKUP";
export const GET_COUNT_PICKUP = "home/types/GET_COUNT_PICKUP";
export const RESET_PICKUP = "home/types/RESET_PICKUP";

export const GET_PICKUP_DETAILS_PROGRESS = "home/types/GET_PICKUP_DETAILS_PROGRESS";
export const GET_PICKUP_DETAILS = "home/types/GET_PICKUP_DETAILS";
export const GET_PICKUP_DETAILS_FAILED = "home/types/GET_PICKUP_DETAILS_FAILED";

export const SET_CAMERA = "home/types/SET_CAMERA";
export const IMAGE_LOADING = "home/types/IMAGE_LOADING";
export const ADD_IMAGE = "home/types/ADD_IMAGE";
export const RESET_IMAGE = "home/types/RESET_IMAGE";
export const SET_INPUT = "home/types/SET_INPUT";
export const SAVE_LAT_LONG = "home/types/SAVE_LAT_LONG";

export const RESET_FORPICKUP = "home/types/RESET_FORPICKUP";
export const SET_BOXES_IMAGES = "home/types/SET_BOXES_IMAGES";
export const SET_UNIQUE_UUID = "home/types/SET_UNIQUE_UUID";

export const SET_DATA_FOR_DUPLICATE = "home/types/SET_DATA_FOR_DUPLICATE";
export const DELETE_RECORD_FROM_SQL = "home/types/DELETE_RECORD_FROM_SQL";

