/* eslint-disable max-len */
import * as Types from "./types";
import api from "../api/index";
import _ from "lodash";

export const setLoginDetails = (data) => ({
	type: Types.SET_LOGIN_DETAILS,
	data,
});

export const isRequiredPIN = (data) => ({
	type: Types.SET_REQUIRED_PIN,
	data,
});

export const resetLogin = () => ({
	type: Types.RESET_LOGIN,
});

export const logout = () => ({
	type: Types.LOGOUT,
});

export const setLoginCredential = (data) => ({
	type: Types.SET_LOGIN_CREDENTIAL,
	data,
});

export const updateLogin = (data) => ({
	type: Types.LOGIN_SUCCESS,
	data,
});

export const getAllBU = (data) => ({
	type: Types.GET_ALL_BU,
	data,
});

export const login = (params, session, navigation) => (
	async(dispatch) => {
		try {
			dispatch({ type: Types.LOGING_INPROGRESS});

			const response = await api.login(params);

			const SessionPIN = _.has(session, "PIN") ? session.PIN : "";

			if (response && response.status === 1){
				dispatch({ type: Types.LOGIN_SUCCESS, data: {...response, PIN: SessionPIN}});
				navigation.navigate("CreatePINScreen", {title: "Create PIN"});
			} else {
				throw response;
			}
		} catch (error) {
			dispatch({ type: Types.LOGIN_FAILED, error: error.result || error.message || "Invalid email or password."});
		}
	}
);

export const getAllAgent = (params) => (
	async(dispatch) => {
		try {
			dispatch({ type: Types.LOGIN_AS_LOAD});

			const response = await api.callPost("/login_callback", params);

			if (response){
				dispatch({ type: Types.LOGIN_AS, data: response});
			}
		} catch (error) {
			dispatch({ type: Types.LOGIN_AS_FAILED, error});
		}
	}
);
