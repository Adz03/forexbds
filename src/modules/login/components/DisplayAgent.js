/* eslint-disable */
import Resource from "__proj/src/resources";
import React, { PureComponent } from "react";
import { Text, FlatList, StatusBar } from "react-native";
import PropTypes from "prop-types";
import {ListItem} from "react-native-elements";
import styles from "../styles.css";
import _ from "lodash";
import * as Animatable from 'react-native-animatable';
import TxtInput from "__src/components/TxtInput";
const {Color} = Resource;

class DisplayAgent extends PureComponent {
	constructor(props){
		super(props);
		
		const {login: {loginAsFromBU}} = props;
		const SortedBU = _.orderBy(loginAsFromBU.result, ["first_name"], ["asc"]);

		this.state = {
			viewp: true, 
			newSearch: SortedBU || [],
			message: "",
			error: {},
			user: {},
			search: ""
		};
	}

	onItemClick = (item) => {
		const {actions, navigation} = this.props;

		actions.setLoginCredential(item);
		navigation.navigate("Home");
	}
    
	_renderItem = ({item, index}) => {
		return (
			<ListItem
				onPress={() => this.onItemClick(item)}
				containerStyle={styles.listItemContainer}
				key={`icx${index}`}
				title={`${item.first_name} ${item.last_name}`}
				titleStyle={{fontFamily: "Roboto-Light", fontSize: 17}}
			/>
		);
	}

	onChangeText = async(value) => {
		const {login: {loginAsFromBU}} = this.props;
		let data;
		if(_.isEmpty(value)){
			data = _.orderBy(loginAsFromBU.result, ["first_name"], ["asc"]);
		}else{
			const res = await _.filter(loginAsFromBU.result, item => {
				return item.first_name.toLowerCase().startsWith(value.toLowerCase()) || item.last_name.toLowerCase().startsWith(value.toLowerCase());
			})
			data = res;
		}

		this.setState({search: value, newSearch: data});
	}

	render() {
		const {newSearch, search} = this.state;

		return (
			<Animatable.View animation="fadeInRight" style={[styles.flex1,{paddingHorizontal: 20}]}>
				<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
				<Text style={[styles.txtLoginas, {marginTop: 20}]}>Login As Agent:</Text>
				<TxtInput
					onRef={(e) => this.email = e }
					onFocus={() => this.setState({usernameFocus: true})}
					onBlur={() => this.setState({usernameFocus: false})}
					round
					value={search}
					style={[styles.marginTop10, styles.marB10]}
					placeholder="Search here..."
					isFocus={this.state.usernameFocus}
					onChangeText={this.onChangeText}
					returnKeyType='next'
					onPass={() => this.onChangeText("")}
					/>
				<FlatList
					data={newSearch}
					extraData={this.state}
					keyExtractor={(item, idx) => `idx${idx}`}
					renderItem={this._renderItem}/>
			</Animatable.View>
		);
	}
}

DisplayAgent.propTypes = {
	actions: PropTypes.object,
	session: PropTypes.object,
	navigation: PropTypes.object,
	login: PropTypes.object,
};

export default DisplayAgent;
