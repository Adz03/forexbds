/* eslint-disable */
import React, { PureComponent } from "react";
import { Text, FlatList, StatusBar } from "react-native";
import PropTypes from "prop-types";
import {ListItem} from "react-native-elements";
import styles from "../styles.css";
import _ from "lodash";
import LoadingModal from "../../../components/LoaderModal";
import * as Animatable from 'react-native-animatable';
import TxtInput from "__src/components/TxtInput";
import Resources from "__src/resources";
const {Color} = Resources;

class DisplayBU extends PureComponent {
	constructor(props){
		super(props);

		const {login: {getAllBU}} = props;
		const SortedBU = _.orderBy(getAllBU.bu_list, ["Name"], ["asc"]);

		this.state = {
			viewp: true,
			newSearch: SortedBU || [],
			message: "",
			error: {},
			user: {},
			itemSelected: {}
		};
	}

	componentDidUpdate(prevProps){
		const {navigation, login: {loginAsFromBU}, actions} = this.props;
		const {itemSelected} = this.state;

		if(!_.isEqual(prevProps.login.loginAsFromBU, loginAsFromBU) && !_.isEmpty(loginAsFromBU)){
			if(loginAsFromBU.result.length > 1){
				navigation.navigate("DisplayAgent", {title: itemSelected.Name});
			}else{
				actions.setLoginCredential(loginAsFromBU.result[0]);
				navigation.navigate("Home");
			}
		}
	}

	onItemClick = (item) => {
		const {actions, login: {getAllBU}} = this.props;

		const params = {
			bu_id: item.ID,
			group_id: getAllBU.group_id
		}

		this.setState({itemSelected: item});
		actions.getAllAgent(params)
	}
	
	_renderItem = ({item, index}) => {
		return (
			<ListItem
				onPress={() => this.onItemClick(item)}
				containerStyle={styles.listItemContainer}
				key={`icx${index}`}
				title={item.Name}
				titleStyle={{fontFamily: "Roboto-Light", fontSize: 17}}
			/>
		);
	}

	onChangeText = async(value) => {
		const {login: {getAllBU}} = this.props;
		let data;

		if(_.isEmpty(value)){
			data = _.orderBy(getAllBU.bu_list, ["Name"], ["asc"]);
		}else{
			const res = await _.filter(getAllBU.bu_list, item => {
				return item.Name.toLowerCase().startsWith(value.toLowerCase());
			})
			data = res;
		}

		this.setState({search: value, newSearch: data});
	}

	render() {
		const {login: {isLoginAsLoad}} = this.props;
		const {newSearch, search} = this.state;

		return (
			<Animatable.View animation="fadeInRight" style={[styles.flex1,styles.padH20]}>
				<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
				<Text style={[styles.txtLoginas, styles.marginTop20, styles.marB10]}>What business unit are you?</Text>
				<TxtInput
					onRef={(e) => this.email = e }
					onFocus={() => this.setState({usernameFocus: true})}
					onBlur={() => this.setState({usernameFocus: false})}
					round
					value={search}
					style={[styles.marginTop10, styles.marB20]}
					placeholder="Search here..."
					isFocus={this.state.usernameFocus}
					onChangeText={this.onChangeText}
					returnKeyType='next'
					onPass={() => this.onChangeText("")} />
				<FlatList
					data={newSearch}
					extraData={this.state}
					keyExtractor={(item, idx) => `idx${idx}`}
					renderItem={this._renderItem}/>
				<LoadingModal loading={isLoginAsLoad}/>
			</Animatable.View>
		);
	}
}

DisplayBU.propTypes = {
	actions: PropTypes.object,
	session: PropTypes.object,
	navigation: PropTypes.object,
	login: PropTypes.object,
};

export default DisplayBU;
