/* eslint-disable */
import React, { PureComponent } from "react";
import { Text, FlatList, Alert, StatusBar, BackHandler } from "react-native";
import PropTypes from "prop-types";
import {ListItem} from "react-native-elements";
import styles from "../styles.css";
import _ from "lodash";
import LoadingModal from "../../../components/LoaderModal";
import {StackActions, NavigationActions, HeaderBackButton} from "react-navigation";
import * as Animatable from 'react-native-animatable';
import Resources from "__src/resources";
const {Color} = Resources;

class LoginAs extends PureComponent {
	_willBlurSubscription;
	_didFocusSubscription;

	constructor(props){
		super(props);

		this._didFocusSubscription = props.navigation.addListener("didFocus", () =>
			BackHandler.addEventListener("hardwareBackPress", this.logout)
		);
		this.state = {
			viewp: true,
			newSearch: props.login.session.result.groups || [],
			message: "",
			error: {},
			user: {},
			itemSelected: {}
		};
	}

	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: "Forex Cargo Inc",
			headerLeft: (<HeaderBackButton tintColor="white"
				onPress={navigation.getParam("logout")}/>)
		}
	};

	componentDidMount(){
		const {navigation} = this.props;

		navigation.setParams({ logout: this.logout });

		this._willBlurSubscription = this.props.navigation.addListener("willBlur", () =>
			BackHandler.removeEventListener("hardwareBackPress", this.logout)
		);
	}

	componentWillUnmount() {
		this._willBlurSubscription && this._willBlurSubscription.remove();
		this._didFocusSubscription && this._didFocusSubscription.remove();
	}

	componentDidUpdate(prevProps){
		const {navigation, login: {loginAsFromBU}, actions} = this.props;
		const {itemSelected} = this.state;

		if(!_.isEqual(prevProps.login.loginAsFromBU, loginAsFromBU) && !_.isEmpty(loginAsFromBU)){
			if(loginAsFromBU.result.length > 1){
				navigation.navigate("DisplayAgent", {title: itemSelected.Name});
			}else{
				actions.setLoginCredential(loginAsFromBU.result[0]);
				navigation.navigate("Home");
			}
		}
	}

	logout = () => {
		Alert.alert("Notice", "Want to sign out?",
			[{
				text: "Cancel",
				onPress: () => console.log("Cancel Pressed"),
				style: "cancel",
			}, {text: "OK", onPress: () => this.onLogout()},
		], {cancelable: false});

		return true;
	}

	onLogout = () => {
		const {actions, navigation} = this.props;

		const resetAction = StackActions.reset({
		  index: 0,
		  key: null,
		  actions: [NavigationActions.navigate({routeName: "Login"})],
		});

		actions.logout();
		navigation.dispatch(resetAction);
	}

	onItemClick = (item) => {
		const {actions, navigation, login: {session}} = this.props;

		switch(item.group_id){
			case 50: case 51: case "50":
				Alert.alert("Notice", "Invalid user credential!", [{text: "Ok", onPress: () => navigation.goBack()}], {cancelable: false});
				break;
			case 3: case 4: case "3": case "4":
					if(item.bu_list.length > 1){
						actions.getAllBU(item);
						navigation.navigate("DisplayBU", {title: item.group_name});
					}else{
						const params = {
							bu_id: item.bu_list[0].ID,
							group_id: item.group_id
						}
				
						this.setState({itemSelected: item.bu_list[0]});
						actions.getAllAgent(params)
					}
				break;
			case 5: case "5":
				item.agent_id = _.toString(session.result.user_id);
				actions.setLoginCredential(item);
				navigation.navigate("Home");
				break;
		}
	}
	
	_renderItem = ({item, index}) => {
		return (
			<ListItem
				onPress={() => this.onItemClick(item)}
				containerStyle={styles.listItemContainer}
				key={`icx${index}`}
				title={item.group_name}
				titleStyle={{fontFamily: "Roboto-Light", fontSize: 17}}
			/>
		);
	}

	onChangeText = async(value) => {
		const {login: {session}} = this.props;
		let data;
		if(_.isEmpty(value)){
			data = session.result.groups;
		}else{
			const res = await _.filter(session.result.groups, item => {
				return item.group_name.toLowerCase().startsWith(value.toLowerCase());
			})
			data = res;
		}

		this.setState({search: value, newSearch: data});
	}

	render() {
		const {login: {isLoginAsLoad}} = this.props;
		const {newSearch} = this.state;

		return (
			<Animatable.View animation="fadeInRight" style={[styles.flex1, styles.padH20]}>
				<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
				<Text style={[styles.txtLoginas, styles.marginTop20, styles.marB10]}>Choose group:</Text>
				<FlatList
					data={newSearch}
					extraData={this.state}
					keyExtractor={(item, idx) => `idx${idx}`}
					renderItem={this._renderItem}/>
				<LoadingModal loading={isLoginAsLoad}/>
			</Animatable.View>
		);
	}
}

LoginAs.propTypes = {
	actions: PropTypes.object,
	session: PropTypes.object,
	navigation: PropTypes.object,
	login: PropTypes.object,
};

export default LoginAs;
