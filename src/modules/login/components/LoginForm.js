/* eslint-disable */
import React, { PureComponent } from "react";
import { Text, View, SafeAreaView, Image, PermissionsAndroid,
	Platform, Linking } from "react-native";
import Resource from "__proj/src/resources";
import TxtInput from "__src/components/TxtInput";
import PropTypes from "prop-types";
import Button from "__src/components/Button";
import styles from "../styles.css";
import NetInfo from "@react-native-community/netinfo";
import _ from "lodash";

import KeyboardDismiss from "__src/components/KeyboardDismiss";
const {Res} = Resource;

class LoginForm extends PureComponent {
	static navigationOptions = {
		header: null,
	}

	constructor(props){
		super(props);
		this.state = {
			viewp: true, isConnected: false,
			message: "", error: {}, user: {},
		};
	}
  _subscription = null;

	componentDidMount(){
		this.GetAllPermissions();
		NetInfo.fetch().then((state) => {
			this.setState({ isConnected: state.isConnected });
		});

		this._subscription = NetInfo.addEventListener((state) => {
			this.setState({ isConnected: state.isConnected });
		});
	}

	componentWillUnmount() {
		this._subscription && this._subscription();
	}

	GetAllPermissions = async() => {
		try {
			if (Platform.OS === "android") {
				const userResponse = await PermissionsAndroid.requestMultiple([
					PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
					PermissionsAndroid.PERMISSIONS.CAMERA,
					PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
				]);

				console.log("userResponse", userResponse);

				if (userResponse === PermissionsAndroid.RESULTS.GRANTED) {
					return true;
				}

				return false;
			}
		} catch (err) {
			return false;
		}
	}

	_handleChangeInput = (type) => (value) => {
		const  error  = {};
		const { actions, login } = this.props;
		const newInput = _.merge({}, login.inputLoginDetails);

		switch (type){
		case 1:
			if (_.isEmpty(value)) {
				error.email = "Email is required";
			}
			newInput.email = (value).trim();
			break;
		case 2:
			if (_.isEmpty(value)) {
				error.password = "Password is required";
			}

			newInput.password = (value).trim();
			break;
		}
        
		this.setState({error});
		actions.setLoginDetails(newInput);
	}

	_onSubmit = () => {
		const {actions, login, navigation} = this.props;
		const error = {};

		if (_.isEmpty(login.inputLoginDetails.email)){
			error.email = "Email is required.";
			this.email.focus();
		} else if (_.isEmpty(login.inputLoginDetails.password)){
			error.password = "Password is required.";
			this.password.focus();
		} else if(!this.state.isConnected){
			error.email = "Network error! Please check your data connection.";
		} else if(!this.GetAllPermissions()){
			error.email = "Please allow permissions.";
		}

		if (_.isEmpty(error)){
			const params = {};
			params.email = login.inputLoginDetails.email;
			params.password = login.inputLoginDetails.password;
			
			actions.login(params, login.session, navigation);
		} else {
			this.setState({error});
		}
	}

	gotoForgot = () => {
  	Linking.openURL("https://apps.forexcargo.us/forgot_password")
  		.catch((err) => console.error("An error occurred", err));
	}

	render() {
		const {login: { inputLoginDetails, isLoggingInProg, isLoggedFailed, 
			session}, navigation, onPINSignIn} = this.props;
		const {error} = this.state;

		return (
			<KeyboardDismiss>
				<View style={styles.flex1}>
					<View style={[styles.top]}>
						<Image source={Res.get("Forex_Logo")} 
							style={{width: 170, height: 110}} resizeMode="contain"/>
					</View>

					<View style={[styles.middle]}>
						<TxtInput
							logintype
							onRef={(e) => this.email = e }
							onFocus={() => this.setState({usernameFocus: true})}
							onBlur={() => this.setState({usernameFocus: false})}
							round
							style={[styles.marginTop10, {alignItems: "flex-start"}]}
							placeholder="Email"
							isFocus={this.state.usernameFocus}
							onChangeText={this._handleChangeInput(1)}
							value={inputLoginDetails.email}
							returnKeyType='next'
							icon="profile_icon"
							err={isLoggedFailed || error.email} />

						<TxtInput
							logintype
							style={[styles.marginTop10, {alignItems: "flex-start"}]}
							onRef={(e) => this.password = e }
							onFocus={() => this.setState({passwordFocus: true})}
							onBlur={() => this.setState({passwordFocus: false})}
							placeholder="Password"
							round
							isFocus={this.state.passwordFocus}
							onChangeText={this._handleChangeInput(2)}
							value={inputLoginDetails.password}
							returnKeyType='next'
							secureTextEntry={this.state.viewp}
							icon="lock_icon"
							icon2="view_icon"
							onSubmitEditing={this._onSubmit}
							viewPass={() => this.setState({viewp: !this.state.viewp})}
							err={error.password} />

						<Text style={[styles.txtGetHelpContainer, {}]}>
							<Text  suppressHighlighting
								onPress={this.gotoForgot}> Forgot Password? </Text>
						</Text>

						<View style={[styles.viewLogin]}>
							<Button
								onPress={this._onSubmit}
								loading={isLoggingInProg}
								style={styles.btnlogin}
								style2={styles.btnlogin2}
								label="Sign In"
							/>
						</View>

						{!_.isEmpty(session.PIN) && <Text style={styles.txtOr} suppressHighlighting
          		onPress={onPINSignIn}>Sign in with pin?</Text>}

					</View>
					<SafeAreaView />
				</View>
			</KeyboardDismiss>
		);
	}
}

LoginForm.propTypes = {
	actions: PropTypes.object,
	session: PropTypes.object,
	navigation: PropTypes.object,
	login: PropTypes.object,
};

export default LoginForm;
