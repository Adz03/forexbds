/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable max-len */
import React, { PureComponent } from "react";
import { View, StatusBar, SafeAreaView, PermissionsAndroid, Alert,
	AppState } from "react-native";
import PropTypes from "prop-types";
import styles from "../styles.css";
import LoginForm from "./LoginForm";
import LoginAs from "./LoginAs";
import _ from "lodash";
import Resource from "__src/resources";
import SystemSetting from "react-native-system-setting";
import Geolocation from "@react-native-community/geolocation";
import * as Schema from "../../../controllers/CodeController";
import ModalPINScreen from "./ModalPinScreen";
import {
	handleAndroidBackButton,
	removeAndroidBackButtonHandler,
} from "__src/components/BackHandlerConfig";
const {Color} = Resource;
const geoOptions = { enableHighAccuracy: true, timeout: 10000 };

class LoginScreen extends PureComponent {

	constructor(props){
		super(props);
		this.state = {
			isLoginScreen: true,
			switchpin: false,
			isFromLogin: true, isPinShow: false,
			appState: AppState.currentState,
		};
	}
	focusListener = null;

	async componentDidMount(){
		const {login: {isLoggedIn, setLoginCredential, session}, navigation, actions} = this.props;
		
		this.getLocation();
		AppState.addEventListener("change", this._handleAppStateChange);

		if (isLoggedIn && !_.isEmpty(setLoginCredential)){
			// actions.isRequiredPIN(true);
			navigation.navigate("Home", {title: "PICK-UP"});
		} else if (isLoggedIn && _.isEmpty(session.PIN)){
			navigation.navigate("CreatePINScreen", {title: "Create PIN"});
		} else if (isLoggedIn && _.isEmpty(setLoginCredential) && !_.isEmpty(session.PIN)){
			navigation.navigate("LoginAs", {title: "Choose Group"});
		}

		if (isLoggedIn){
			this.setState({isPinShow: true}, () => console.log("ISLOGGEDIN", this.state.isPinShow));
		}
		

		try {
			const res = await Schema.createPickupDB();

			console.log("Create Pickup DB", res);
		} catch (e){
			
		}
		this.focusListener = navigation.addListener("didFocus", () => {
			handleAndroidBackButton("loginScreen", this.logout);
		});
	}

	componentWillUnmount(){
		this.focusListener && this.focusListener.remove();
		removeAndroidBackButtonHandler("loginScreen");
		AppState.addEventListener("change", this._handleAppStateChange);
	}

	_handleAppStateChange = (nextAppState) => {
		const {login: {isLoggedIn}, home: {setCamera}} = this.props;

		if (this.state.appState.match(/inactive|background/) &&
      nextAppState === "active" && isLoggedIn && !setCamera) {
			console.log("App has come to the foreground!");
			this.setState({isPinShow: true, isFromLogin: false});
		}
		console.log(`App has come to the background!${  nextAppState}`);
		this.setState({appState: nextAppState});
	};

	getLocation = () => {
		return new Promise(async () => {
			if (Platform.OS === "android") {
				const permission = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

				if (permission === PermissionsAndroid.RESULTS.GRANTED) {
					Geolocation.getCurrentPosition(this.geoSuccess, this.geoFailure, geoOptions);
				} else {
					Alert.alert("Permission denied!", "Please allow your location in settings.",
						[{text: "OK", onPress: () => RNExitApp.exitApp()}],
						{cancelable: false});
				}
			} else {
				SystemSetting.isLocationEnabled().then((enable) => {
					this.setState({location: enable});
					if (!enable){
						Alert.alert("Location required!", "Can't get in touch to your location, Please allow it in your settings.",
							[{text: "OK"}],
							{cancelable: false});
					}
				});
				Geolocation.requestAuthorization();
				Geolocation.getCurrentPosition(this.geoSuccess, this.geoFailure, geoOptions);
			}
		});
	}

	geoSuccess = (position) => {
		console.log("geoSuccess", position);
	};

	geoFailure = (err) => {
		console.log("geoFailure", err);
		// Alert.alert("Notice", "Unable to get location. Please enable your location in phone settings.");
	};
	
	renderLoginForm = () => (
		<LoginForm {...this.props} onPINSignIn={this.onPINSignIn} />
	);

	renderLoginAs = () => (
		<LoginAs {...this.props} />
	);

	onPINSignIn = () => {
		this.setState({isPinShow: !this.state.isPinShow});
	}

	render() {
		const {login: {isLoggedIn}} = this.props;
		const {isFromLogin, isPinShow} = this.state;

		return (
			<SafeAreaView style={styles.container}>
				<View style={styles.container}>
					<StatusBar barStyle="dark-content" backgroundColor={Color.white} />
					<View style={styles.body}>
						{isLoggedIn ? this.renderLoginForm() : this.renderLoginForm()}
					</View>
				</View>

				<ModalPINScreen isFromLogin={isFromLogin} visible={isPinShow}
					onClose={() => this.setState({isPinShow: false})} {...this.props}/>
			</SafeAreaView>
		);
	}
}

LoginScreen.propTypes = {
	login: PropTypes.object,
	actions: PropTypes.object,
	navigation: PropTypes.object,
	home: PropTypes.object,
};

export default LoginScreen;
