/* eslint-disable */
import React from "react";
import {View, Text, SafeAreaView, StyleSheet, TouchableOpacity,
	StatusBar, Image, Alert, ScrollView, BackHandler, Modal} from "react-native";
import NumberButton from "./screens/NumberButton";
import Bullet from "./screens/Bullet";
import { StackActions, NavigationActions } from "react-navigation";
import _ from "lodash";
import Resource from "__src/resources";
import PropTypes  from "prop-types";
import CryptoJS from "crypto-js";
const {Res, Color} = Resource;

class ModalPINScreen extends React.PureComponent{

	constructor(props){
		super(props);

		this.state = {
			pincode: "",
			isLoading: false,
			lat: "", long: "",
			isConnected: false,
			location: false, newEnc: "",
		};
	}

	onBack = () => {
		this.props.navigation.goBack();

		return true;
	}

  isBullet = () => {
  	const {pincode} = this.state;

  	if (_.isEmpty(pincode)){
  		return 0;
  	}

  	return pincode.length;
	}
	
	onPress = (value) => {
  	const {pincode} = this.state;
  	const newPin = `${pincode}${value}`;

  	if (pincode.length < 4) {
  		this.setState({pincode: newPin, newEnc: CryptoJS.MD5(newPin)}, () => {
				setTimeout(() => {
					const {login: {session}, actions, isFromLogin, navigation} = this.props;

					if (this.state.pincode.length === 4) {
						if (session.PIN === _.toString(this.state.newEnc)){
							const newinput = _.merge({}, session);

							actions.updateLogin(newinput);
							this.props.onClose();
							this.setState({pincode: ""});
							if(isFromLogin){
								if (session.result.groups.length > 1){
									navigation.navigate("LoginAs", {title: "Login As"});
								} else {
									switch (session.result.groups[0].group_id){
									case 3:
									case 4:
										actions.getAllBU(session.result.groups[0])
										navigation.navigate("DisplayBU", {title: session.result.groups[0].group_name});
										break;
									case 5:
									default:
										const params2 = {...session.result.groups[0]};
							
										params2.agent_id = _.toString(session.result.user_id);

										actions.setLoginCredential(params2);
										navigation.navigate("Home");
										break;
									}
								}
							}

						} else {
							Alert.alert("Notice", "PIN code does not match.");
							this.setState({pincode: ""});
						}
					}
				}, 700);
  		});
  	}
	}

  onPress2 = (value) => {
  	const {pincode} = this.state;
  	const newPin = `${pincode}${value}`;

  	if (pincode.length < 4) {
  		this.setState({pincode: newPin, newEnc: CryptoJS.MD5(newPin)}, () => {
				const timeout = setTimeout(() => {
					const {login: {setPin, setLoginCredential}, delivery: {selectedTrip},
					 actions, navigation, isFromLogin} = this.props;

					if (this.state.pincode.length === 4) {
						if (setPin === _.toString(this.state.newEnc)){
							this.props.onClose();
							actions.turnOnPin();
							if (isFromLogin && !_.isEmpty(setLoginCredential) && !_.isEmpty(selectedTrip)){
								navigation.navigate("Delivery", {title: "DELIVERY"});
							}else if(isFromLogin && _.isEmpty(selectedTrip)){
								navigation.navigate("DisplayTripNumber");
							}
							this.setState({pincode: ""});
						} else {
							Alert.alert("Notice", "PIN code does not match.");
							this.setState({pincode: ""});
						}
					}

					clearTimeout(timeout);
				}, 400);
  		});
  	}
	}

  delete = () => {
  	const {pincode} = this.state;

  	if (pincode.length > 0){
  		const res = pincode.slice(0, pincode.length - 1);

  		this.setState({pincode: res});
  	}
	}
	
	onLogout = () => {
		const {actions, navigation, onClose} = this.props;
		const resetAction = StackActions.reset({
		  index: 0,
		  key: null,
		  actions: [NavigationActions.navigate({routeName: "Login"})],
		});

		actions.logout();
		navigation.dispatch(resetAction);
		onClose();
	}

	onRequestClose = () => {
		Alert.alert("Notice", "Do you wish to exit the app?",
			[{text: "Yes", onPress: () => BackHandler.exitApp()},{ text: "No" }],
			{cancelable: false});
	}

  render(){
  	const { visible} = this.props;

  	return (
			<Modal
				animationType="slide"
				visible={visible}
				onRequestClose={this.onRequestClose}>
				<SafeAreaView style={styles.container}>
					<View style={styles.container}>
						<StatusBar barStyle="dark-content" backgroundColor={Color.Header} />
						<ScrollView style={styles.view2}>
							<View style={[styles.containerLogo, styles.marT15]}>
								<Image	source={Res.get("user_icon")}
									style={styles.imageLogo}/>
							</View>
							<Text style={[styles.txtHeader]}>Your pin is?</Text>
							<View style={[styles.view1, styles.marT15]}>
								<Bullet active={this.isBullet() >= 1}/>
								<Bullet active={this.isBullet() >= 2}/>
								<Bullet active={this.isBullet() >= 3}/>
								<Bullet active={this.isBullet() >= 4}/>

								<TouchableOpacity activeOpacity={0.8} style={{position: "absolute", right: 15}} onPress={this.delete}>
									<Image style={{width: 30, height: 30}} source={Res.get("ic_erase")}/>
								</TouchableOpacity>
							</View>
							<View style={[styles.view1, styles.marT20]}>
								<NumberButton label={"1"} onPress={this.onPress} />
								<NumberButton label={"2"} style={styles.marH15} onPress={this.onPress} />
								<NumberButton label={"3"} onPress={this.onPress}/>
							</View>
							<View style={[styles.view1, styles.marT10]}>
								<NumberButton label={"4"} onPress={this.onPress} />
								<NumberButton label={"5"} style={styles.marH15} onPress={this.onPress} />
								<NumberButton label={"6"} onPress={this.onPress} />
							</View>
							<View style={[styles.view1, styles.marT10]}>
								<NumberButton label={"7"} onPress={this.onPress} />
								<NumberButton label={"8"} style={styles.marH15} onPress={this.onPress} />
								<NumberButton label={"9"} onPress={this.onPress} />
							</View>
							<View style={[styles.view1, styles.marT10]}>
								<NumberButton label={"0"} onPress={this.onPress} />
							</View>
						</ScrollView>
						<View style={styles.viewAction}>
							<Text suppressHighlighting
								style={styles.textStyle}
								onPress={this.onLogout}>Login Using Password</Text>
						</View>
					</View>
				</SafeAreaView>
			</Modal>
  	);
  }
}

ModalPINScreen.propTypes = {
	onPress: PropTypes.func,
	login: PropTypes.object,
	home: PropTypes.object,
	actions: PropTypes.object,
	navigation: PropTypes.object,
	onCancel: PropTypes.func,
	onForgotPIN: PropTypes.func,
	onChangePIN: PropTypes.func,
};

const styles = StyleSheet.create({
	container: {flex: 1, backgroundColor: "white"},
	textStyle: {fontFamily: "Roboto-Light", fontSize: 18},
	txtHeader: {padding: 10, fontFamily: "Roboto-Light", fontSize: 20, textAlign: "center"},
	view1: {flexDirection: "row", alignItems: "center", justifyContent: "center"},
	marH15: {marginHorizontal: 15},
	containerLogo: { alignItems: "center", justifyContent: "flex-end", marginBottom: 10  },
	viewAction: {flexDirection: "row", padding: 22, bottom: 5, alignItems: "center",
	 justifyContent: "space-between"},
	marT10: { marginTop: 10},
	marT15: { marginTop: 15},
	marT20: { marginTop: 20},
	imageLogo: {width: 80, height: 80},
	view2: {flex: 1},
});

export default ModalPINScreen;
