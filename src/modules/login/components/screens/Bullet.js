import React from "react";
import {View, StyleSheet} from "react-native";
import Resource from "__src/resources";
import PropTypes  from "prop-types";
const {Color} = Resource;

class Bullet extends React.PureComponent{
	render(){
		const {active} = this.props;
		const style = active ? {backgroundColor: Color.colorPrimary} : null;

		return (
			<View style={[styles.container, style]} />
		);
	}
}
const styles = StyleSheet.create({
	container: {width: 25, height: 25, borderRadius: 25 / 2, backgroundColor: "white",
		borderWidth: 1, borderColor: Color.colorPrimary, margin: 7},
});

Bullet.propTypes = {
	active: PropTypes.bool,
};

export default Bullet;
