/* eslint-disable */
import React, {PureComponent} from "react";
import {View, Text, TouchableOpacity,StatusBar, 
	StyleSheet, SafeAreaView, Keyboard} from "react-native";
import TxtInput from "__src/components/TxtInput";
import KeyboardDismiss from "__src/components/KeyboardDismiss";
import Resources from "__src/resources";
import {Icon} from "react-native-elements";
import PropTypes from "prop-types";
import CryptoJS from "crypto-js";
import _ from "lodash";
const {Color} = Resources;
const errorMessage = "This field is required."

export default class CreatePINScreen extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			error: {},
			setInput: {}
		};
	}

	_onBack() {
		const {goBack} = this.props.navigation;

		goBack();
	}

	_onChangeText = (type) => (val) =>{
		const {setInput} = this.state;
		const error = {};
		const newInput = _.merge({}, setInput);
		
		if (_.isEmpty(val)) {
			error[type] = "This field is required.";
		}

		newInput[type] = val;
		this.setState({error, setInput: newInput});
	}

	_submit = () => {
		const {actions, login: {session}, navigation} = this.props;
		const {setInput} = this.state;
		const newInput = _.merge({}, session);
		const error = {};

		if(_.isEmpty(setInput.pin)){
			error.pin = errorMessage;
		}else if(_.isEmpty(setInput.repin)){
			error.repin = errorMessage;
		}else if(setInput.repin !== setInput.pin){
			error.repin = "PIN doesn't match.";
		}

		this.setState({ error });

		if (_.isEmpty(error)) {
			newInput.PIN = _.toString(CryptoJS.MD5(setInput.pin));
			actions.updateLogin(newInput);
			if(session.result.groups.length > 1){
				navigation.navigate("LoginAs", {title: "Login As"});
			}else{
				switch(session.result.groups[0].group_id){
					case 3:
					case 4:
						actions.getAllBU(session.result.groups[0]);
						navigation.navigate("DisplayBU", {title: session.result.groups[0].group_name});
						break;
					case 5:
					default:
						const params = {...session.result.groups[0]};

						params.agent_id = _.toString(session.result.user_id);
						actions.setLoginCredential(params);
						navigation.navigate("Home");
						break;
				}
			}
		}
	}

	onClose = () => {
		const {actions, navigation} = this.props;

		actions.logout();
		navigation.goBack();
	}

	render() {
		const {error, setInput} = this.state;
		
		return (
			<KeyboardDismiss>
			<SafeAreaView style={styles.container}>
				<View style={styles.container}>
				<StatusBar barStyle="dark-content" backgroundColor="white" />
					<TouchableOpacity style={styles.btnClose} onPress={this.onClose} >
						<Icon name='close' 
							color={Color.black} size={35} />
					</TouchableOpacity>
					<View keyboardShouldPersistTaps='handled' 
						style={styles.bodyContainer}>
						<View style={styles.margin30}>
							<Text style={styles.labelText}>Set your PIN!</Text>
						</View>

						<TxtInput
							onChangeText={this._onChangeText("pin")}
							onFocus={() => this.setState({fnFocus: true})}
							onBlur={() => this.setState({fnFocus: false})}
							isFocus={this.state.fnFocus}
							value={setInput.pin}
							onRef={(e) => this.pin = e }
							secureTextEntry
							maxLength={4}
							keyboardType={'numeric'}
							returnKeyType={'next'}
							refname={this.repin}
							err={error.pin}
							label='Enter your pin'
							style={{marginTop: 30}} />

						<TxtInput
							onChangeText={this._onChangeText("repin")}
							onFocus={() => this.setState({lnFocus: true})}
							onBlur={() => this.setState({lnFocus: false})}
							isFocus={this.state.lnFocus}
							value={setInput.repin}
							onRef={(e) => this.repin = e }
							secureTextEntry
							keyboardType={'numeric'}
							returnKeyType={'done'}
							maxLength={4}
							onSubmitEditing={() => { Keyboard.dismiss(); }}
							err={error.repin}
							label='Re-enter your pin'
							// onSubmitEditing={this._submit}
							style={{marginTop: 20}} />
					</View>
					<TouchableOpacity style={styles.btnRoundArrow} onPress={this._submit} >
						<Icon reverse name='angle-right' type='font-awesome' 
							color={Color.colorPrimary} size={35} />
					</TouchableOpacity>
				</View>
			</SafeAreaView>
			</KeyboardDismiss>
		);
	}
}

const styles = StyleSheet.create({
	container: { flexShrink: 1, width: "100%", height: "100%" },
	bodyContainer: {paddingHorizontal: 30, flex: 1, backgroundColor: "#FFF"},
	btnRoundArrow: {position: "absolute", bottom: 10, right: 10},
	btnClose: { alignSelf: "flex-end", top: 10, right: 5, padding: 20},
	margin30: {marginTop: 30},
	labelText: {fontSize: 25, fontFamily: "Roboto", color: Color.Standard2},

})
CreatePINScreen.propTypes = {
	actions: PropTypes.object,
	InputedData: PropTypes.object,
	navigation: PropTypes.object,
};
