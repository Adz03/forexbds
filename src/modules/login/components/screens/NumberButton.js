import React from "react";
import {Text, TouchableWithoutFeedback, StyleSheet, Animated} from "react-native";
import PropTypes  from "prop-types";

class NumberButton extends React.PureComponent{
	constructor(props){
		super(props);

		this.handlePressIn = this.handlePressIn.bind(this);
		this.handlePressOut = this.handlePressOut.bind(this);
	}

	componentWillMount(){
		this.animatePress = new Animated.Value(1);
	}
		
	handlePressIn(){
    	Animated.spring(this.animatePress, {
    		toValue: 0.94,
    	}).start();
	}
		
	handlePressOut(){
    	Animated.spring(this.animatePress, {
    		toValue: 1,
    	}).start();
	}
  
	render(){
		const {onPress, style, label} = this.props;
		const animatedStyle = {
			transform: [{ scale: this.animatePress}],
		};
    
		return (
			<TouchableWithoutFeedback
				onPress={() => onPress(label)}
				onPressIn={this.handlePressIn}
				onPressOut={this.handlePressOut}>
				<Animated.View style={[styles.container, animatedStyle, style]}>
					<Text style={styles.txtStyle}>{label}</Text>
				</Animated.View>
			</TouchableWithoutFeedback>
		);
	}
}

NumberButton.propTypes = {
	onPress: PropTypes.func,
	style: PropTypes.object,
	label: PropTypes.string,
};

const styles = StyleSheet.create({
	container: {width: 70, height: 70, borderRadius: 35, backgroundColor: "rgba(0,0,0,0.4)",
		alignItems: "center", justifyContent: "center"},
	txtStyle: {fontFamily: "Roboto", fontSize: 25, fontWeight: "bold", color: "white"},
});

export default NumberButton;
