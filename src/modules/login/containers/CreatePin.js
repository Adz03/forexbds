import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import CreatePINScreen from "../components/screens/CreatePINScreen";

const mapStateToProps = ({ login, nav }) => ({
	login,
	nav,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(ActionCreators, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePINScreen);
