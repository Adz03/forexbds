import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import DisplayAgent from "../components/DisplayAgent";

const mapStateToProps = ({ login, nav }) => ({
	login,
	nav,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(ActionCreators, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(DisplayAgent);
