import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import DisplayBU from "../components/DisplayBU";

const mapStateToProps = ({ login, nav }) => ({
	login,
	nav,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(ActionCreators, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(DisplayBU);
