import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import LoginAs from "../components/LoginAs";

const mapStateToProps = ({ login, nav, delivery }) => ({
	login,
	nav,
	delivery,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({...ActionCreators}, dispatch),
	dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginAs);
