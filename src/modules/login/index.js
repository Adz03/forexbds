import Login from "./containers/Login";
import LoginAs from "./containers/LoginAs";
import DisplayBU from "./containers/DisplayBU";
import DisplayAgent from "./containers/DisplayAgent";
import CreatePINScreen from "./containers/CreatePin";
import navigationOptions from "__src/components/navOpt";
import reducers from "./reducers";

export const login = reducers;
export default {
	Login: {
		screen: Login,
		navigationOptions: {
			header: null,
		},
	},
	DisplayAgent: {
		screen: DisplayAgent,
		navigationOptions,
	},
	LoginAs: {
		screen: LoginAs,
		navigationOptions,
	},
	DisplayBU: {
		screen: DisplayBU,
		navigationOptions,
	},
	CreatePINScreen: {
		screen: CreatePINScreen,
		navigationOptions: {
			header: null,
		},
	},
};
