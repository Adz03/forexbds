import { combineReducers } from "redux";
import * as Types from "./types";

const initialInput = {
	// email: "arjay@creativequoin.com",
	// password: "pa55word",
	email: "",
	password: "",
	logtype: 1,
};

const inputLoginDetails = (state = initialInput, action) => {
	switch (action.type){
	case Types.SET_LOGIN_DETAILS:
		return action.data;
	case Types.LOGIN_SUCCESS:
	case Types.LOGOUT:
		return initialInput;
	default:
		return state;
	}
};

const isLoggingInProg = (state = false, action) => {
	switch (action.type){
	case Types.LOGING_INPROGRESS:
		return true;
	case Types.LOGIN_SUCCESS:
	case Types.LOGIN_FAILED:
		return false;
	default:
		return state;
	}
};

const isLoggedIn = (state = false, action) => {
	switch (action.type){
	case Types.LOGIN_SUCCESS:
		return true;
	case Types.LOGIN_FAILED:
	case Types.LOGING_INPROGRESS:
	case Types.RESET_LOGIN:
	case Types.LOGOUT:
		return false;
	default:
		return state;
	}
};

const isLoggedFailed = (state = "", action) => {
	switch (action.type){
	case Types.LOGIN_FAILED:
		return action.error;
	case Types.LOGIN_SUCCESS:
	case Types.LOGING_INPROGRESS:
	case Types.RESET_LOGIN:
	case Types.LOGOUT:
		return "";
	default:
		return state;
	}
};

const session = (state = {}, action) => {
	switch (action.type){
	case Types.LOGIN_SUCCESS:
		return action.data;
	case Types.RESET_LOGIN:
		return {};
	default:
		return state;
	}
};

const setLoginCredential = (state = {}, action) => {
	switch (action.type){
	case Types.SET_LOGIN_CREDENTIAL:
		return action.data;
	default:
		return state;
	}
};

const isLoginAsLoad = (state = false, action) => {
	switch (action.type){
	case Types.LOGIN_AS_LOAD:
		return true;
	case Types.LOGIN_AS_FAILED:
	case Types.LOGIN_AS:
	case Types.LOGOUT:
		return false;
	default:
		return state;
	}
};

const loginAsFromBU = (state = {}, action) => {
	switch (action.type){
	case Types.LOGIN_AS:
		return action.data;
	case Types.LOGIN_AS_FAILED:
	case Types.LOGIN_AS_LOAD:
	case Types.LOGOUT:
		return {};
	default:
		return state;
	}
};

const isRequiredPIN = (state = false, action) => {
	switch (action.type){
	case Types.SET_REQUIRED_PIN:
		return action.data;
	default:
		return state;
	}
};

const getAllBU = (state = {}, action) => {
	switch (action.type){
	case Types.GET_ALL_BU:
		return action.data;
	case Types.LOGOUT:
		return {};
	default:
		return state;
	}
};

export default combineReducers({
	inputLoginDetails,
	isLoggingInProg,
	isLoggedIn,
	isLoggedFailed,
	session,
	setLoginCredential,
	isLoginAsLoad,
	loginAsFromBU,
	isRequiredPIN,
	getAllBU,
});
