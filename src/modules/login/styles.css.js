import {StyleSheet} from "react-native";
import Color from "__src/resources/styles/color";

export default StyleSheet.create({
	container: {flexShrink: 1, width: "100%", height: "100%", backgroundColor: Color.white},
	body: {flex: 1, backgroundColor: Color.white},
	imagebackground: {width: "100%", height: "100%", position: "absolute"},
	
	// LoginForm
	flex1: {flex: 1},
	flexRow: {flexDirection: "row"},
	txtLanguage: {color: "white", fontFamily: "Roboto-Light", fontSize: 13, marginRight: 3},
	marginTop10: {marginTop: 10},
	marginTop20: {marginTop: 20},
	marB10: {marginBottom: 10},
	marB20: {marginBottom: 20},
	padH20: {paddingHorizontal: 20},
	height90: {height: "93%"},

	top: {width: "100%", height: "35%", alignItems: "center",  justifyContent: "center"},
	middle: {width: "100%", height: "65%", paddingHorizontal: 25},
	bottom: {flex: 1,  height: "20%" },

	txtError: { color: "red", fontFamily: "Roboto-Light", fontSize: 15, textAlign: "center", marginBottom: 5},
	txtHelpContainer: {fontSize: 13, fontFamily: "Roboto-Light", marginVertical: 10, color: Color.white, textAlign: "center" },
	txtHelp: {fontWeight: "bold", color: Color.colorPrimary},
	txtGetHelpContainer: {fontSize: 15, marginVertical: 20, color: Color.colorPrimary, fontFamily: "Roboto-Light", textAlign: "center" },
	txtGetHelpContainer2: {fontSize: 14, marginVertical: 20, color: Color.Standard2, fontFamily: "Roboto-Light", textAlign: "center" },
	txtfb: {color: Color.blue, fontSize: 15, fontFamily: "Roboto-Light"},
	btnfb: {height: 45, marginTop: 15, alignItems: "center",
		backgroundColor: "transparent", flexDirection: "row", justifyContent: "center" },
	loadview: {width: 10, height: 10},

	btnlogin: {height: 50, borderRadius: 25, width: "100%"},
	btnlogin2: {height: 50, borderRadius: 25},
	btncancel: {height: 40, borderBottomWidth: 6, width: 100, backgroundColor: Color.white,
		borderColor: Color.colorPrimaryDark, borderWidth: 1},


	viewLogin: { marginTop: 30},
	txtOr: {fontSize: 16, fontWeight: "500", color: Color.colorPrimary, marginTop: 20,
		fontFamily: "Roboto", textAlign: "center" },

	// LandScape
	flexDirRow: {flexDirection: "row"},
	row1: {flex: 1, alignItems: "center",  justifyContent: "center"},
	row2: {flex: 1, alignItems: "center",  justifyContent: "center", paddingHorizontal: 20},
	txtLifeline: {fontFamily: "Roboto", fontSize: 30, color: Color.black, textAlign: "center"},
	txtLifeline2: {fontFamily: "Roboto", fontSize: 65, color: Color.black, textAlign: "center"},

	txtLoginas: {fontFamily: "Roboto", fontSize: 25,
		color: Color.black, fontWeight: "bold", textAlign: "left"},
	buttonWrapper: { marginTop: 15, paddingHorizontal: 25, flexDirection: "row", justifyContent: "space-evenly"},

	// renderError
	inpuView1: {justifyContent: "center", backgroundColor: Color.lightred, padding: 7, marginTop: 10, flexDirection: "row"},
	iconContainerStyle: {position: "absolute", left: 5, top: 8},
	txt3: {textAlign: "center", fontFamily: "Roboto-Light", color: Color.Standard2, fontSize: 13},

	// Terms and Condition
	listItemContainer: {paddingVertical: 15, paddingLeft: 0},
});
