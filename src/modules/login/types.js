
export const LOGOUT = "home/types/LOGOUT";
export const SET_LOGIN_DETAILS = "login/types/SET_LOGIN_DETAILS";


export const LOGING_INPROGRESS = "login/types/LOGING_INPROGRESS";
export const LOGIN_SUCCESS = "login/types/LOGIN_SUCCESS";
export const LOGIN_FAILED = "login/types/LOGIN_FAILED";

export const RESET_LOGIN = "login/types/RESET_LOGIN";
export const SET_LOGIN_CREDENTIAL = "login/types/SET_LOGIN_CREDENTIAL";
export const SET_REQUIRED_PIN = "login/types/SET_REQUIRED_PIN";

export const LOGIN_AS_LOAD = "login/types/LOGIN_AS_LOAD";
export const LOGIN_AS = "login/types/LOGIN_AS";
export const LOGIN_AS_FAILED = "login/types/LOGIN_AS_FAILED";
export const GET_ALL_BU = "login/types/GET_ALL_BU";
