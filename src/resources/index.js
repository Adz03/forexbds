/* eslint-disable */
let instance = null;

import Color from "./styles/color";
import _ from "lodash";

class ImgResource {
	static getInstance() {
		if (!instance) {
			instance = new ImgResource();
		}
		
		return instance;
	}

	constructor() {
		
		this.imgs = {

			check_icon: require("./images/check_icon.png"),
			view_icon: require("./images/view_icon.png"),
			lock_icon: require("./images/lock_icon.png"),
			date_icon: require("./images/date_icon.png"),
			background_light: require("./images/background_light.png"),
			background_dark: require("./images/background_dark.png"),
			securecircle: require("./images/securecircle.png"),
			mapcircle: require("./images/mapcircle.png"),
			image_file: require("./images/image_file.png"),
			profile_icon: require("./images/profile_icon.png"),
			qr_dark: require("./images/qr_dark.png"),
			qr_frame: require("./images/qr_frame.png"),
			camera_active: require("./images/camera_active.png"),
			user_icon: require("./images/user_icon.png"),
			Forex_Logo: require("./images/Forex_Logo.png"),
			box_img: require("./images/box_img.png"),

		};

		this.string = {
			// PHP: "Philippine Wallet",
			// HKD: "Hongkong Wallet",
			// SGD: "Singapore Wallet",
			// AED: "United States Emirate Wallet",
		};
	}

	get(name) {
		return this.imgs[name];
	}

	getString(name) {
		return this.string[name];
	}

	getSVG(name) {
		return this.svg[name];
	}
}

const Resource = {
	Color, Res: ImgResource.getInstance(),
};

export default Resource;
