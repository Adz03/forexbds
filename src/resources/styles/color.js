

export default {
	colorPrimary: "#253C77",
	colorPrimaryDark: "#1e3162",
	colorPrimaryLight: "#637EB0",
	StatusBar: "#090b0c",
	Header: "#191D21",
	colorAccent: "#FF4081",
	torquoise: "#009688",
	black: "#000000",
	lightBlack: "#484848",
	lightGreen: "#19A992",
	LightBlue: "#20A0FF",
	LightBlue2: "#e8eff7",
	LightBlue4: "#006dcc",
	white: "#ffffff",
	green: "#13c966",
	green01: "#008388",
	green02: "#02656b",
	lightgreen: "#13c966",
	darkOrange: "#d93900",
	lightgray: "#d8d8d8",
	pink: "#fc4c54",
	gray01: "#f3f3f3",
	gray02: "#919191",
	gray03: "#b3b3b3",
	gray04: "#484848",
	gray05: "#808080",
	gray06: "#e2e5e8",
	gray07: "#f2f2f2",
	brown01: "#ad8763",
	orange: "#ff8533",
	brown02: "#7d4918",
	blue: "#0071ba",
	purple: "#7a44cf",
	red: "#FF1B00",
	lightred: "#ffcccc",
	red01: "#c62916",
	translucent: "rgba(255,255,255,0.9)",
	transparent: "transparent",
	lineColor: "#CECECE",
	txt: "#262626",
	border: "#999999",
	def: "#E4E3EB",
	LightDark: "#C0CCDA",
	DarkBG: "#1f354f",
	Standard: "#7b929d",
	Standard2: "#1f354f",
	Standard3: "#949fb1",
	bg: "#eef5f9",
	acct: "#5f7382",
	highlight: "#d2d6d8",
};
